import numpy as np
import pickle
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.lines as mline
import ipdb
import sys
from statsmodels.distributions.empirical_distribution import ECDF
from scipy.stats import pareto


def plotMeanCI( x, data, color, label, ax, with_ci=True ):
    n,M = data.shape
    mean = np.mean(data,axis=0)
    #s = np.std(data,axis=0)
    #high_bound = m+1.96*s/np.sqrt(n)
    #low_bound  = m-1.96*s/np.sqrt(n)

    if with_ci:
        quart = np.percentile(data,q=[25,50,75],axis=0)
        low_bound  = quart[0]
        median     = quart[1]
        high_bound = quart[2]
        rgb_color = np.array(colors.ColorConverter().to_rgb(color))
        rgb_color = list((1-rgb_color)*0.5+rgb_color)
        ax.fill_between(x, high_bound, low_bound, color=colors.rgb2hex(rgb_color), alpha=0.5)
        ax.plot(x,median,color+'-',label=label)
        ax.plot(x,mean,color+'--')
    else:
        ax.set_xscale('log')
        ax.plot(x,mean,color+'--',label=label)

    #ax.set_ylim(bottom=1)

dist_initials = ['b']
func_names    = ["vz","catoni","ucb1_normal","thompson","ucb1"]
func_names    = ["modified_ts2","ts"]

initial2distrib = {"p":"Pareto","z":"Zipf","g":"Geometric"}
function2name   = {
    "vz":"Vakili-Zhao",
    "catoni":"Robust UCB (Catoni)",
    "ucb1_normal":"UCB1-Normal",
    "thompson": "Thompson",
    "ucb1":"UCB1","robust_ucb":"Robust UCB (MoM)",
    "modified_ts":"Modified TS","ts":"Thompson Sampling",
    "modified_ts2":"Modified TS 2"}

func2style = {'catoni':'ro','ucb1_normal':'b*','ucb1':'g^','vz':'cd',
        'thompson':'mv','ts':'b*','modified_ts':'ro','modified_ts2':'go'}

MAX_IDX = 1
MAX_JDX = 1
extension = '.rtxt'
xlabel_dict = {'.rtxt':'E[regret]','.atxt':'# pulls to wrong arm'}
idx = 1
jdx = 2

def idx2mean(idx):
    pareto_xmin = 2.0
    return float(pareto.stats(2.1+idx*0.1,scale=pareto_xmin,moments='m'))


if MAX_IDX > 1 and MAX_JDX > 1:
    fig,ax = plt.subplots(MAX_IDX,MAX_JDX-1,sharey=True)
    fig.set_size_inches(3*MAX_IDX,3*(MAX_JDX-1))
    for idx in xrange(MAX_IDX):
        for jdx in xrange(MAX_JDX):
            if jdx >= idx+1:
                print idx, jdx
                for kdx,initial in enumerate(dist_initials):
                    for function in func_names:
                        actions = np.loadtxt(initial+'_'+function+'_idx'+str(idx)+'_jdx'+str(jdx)+extension)
                        RUNS = len(actions)
                        print '{}: mean {}, max {}'.format(function, np.mean(actions), np.max(actions))
                        ##plot reward distribution at the end
                        ecdf = ECDF(actions)
                        ax[idx,jdx-1].plot( ecdf.x, ecdf.y, func2style[function][0]+'-', linewidth=2 )
                        ax[idx,jdx-1].set_title(r'$\alpha={}$ $\beta={}$'.format(2.1+0.1*idx,2.1+0.1*jdx))
                        ax[idx,jdx-1].yaxis.grid(True)
                print ''

            ax[idx,jdx-1].set_ylim(bottom=0,top=1.05)
            ax[idx,jdx-1].set_xscale('log')
            #ax[idx,jdx-1].set_xlim(left=0,right=100)
            #ax[idx,jdx-1].set_xticks([])

    list_handlers = []
    for k,v in func2style.items():
        list_handlers.append(mline.Line2D([],[], color=v[0], linewidth=2 ))

    plt.figlegend(handles=list_handlers, labels=[function2name[f] for f in func2style.keys()], loc='upper left')
    fig.text(0.5, 0.04, xlabel_dict[extension], ha='center', size='xx-large')
    fig.text(0.04, 0.5, 'CCDF', va='center', rotation='vertical', size='xx-large')

elif MAX_JDX > 1:
    fig,ax = plt.subplots(MAX_IDX,MAX_JDX-1,sharey=True)
    fig.set_size_inches(3*(MAX_JDX-1),3*MAX_IDX)
    idx = 0
    for jdx in xrange(idx+1,MAX_JDX):
        print idx, jdx
        for kdx,initial in enumerate(dist_initials):
            for function in func_names:
                actions = np.loadtxt(initial+'_'+function+'_idx'+str(idx)+'_jdx'+str(jdx)+extension)
                RUNS = len(actions)
                ##plot reward distribution at the end
                ecdf = ECDF(actions)
                ax[jdx-1].plot( ecdf.x, ecdf.y, func2style[function][0]+'-', linewidth=2 )
                ax[jdx-1].set_title(r'$\alpha={}$ $\beta={}$'.format(2.1+0.1*idx,2.1+0.1*jdx))
                ax[jdx-1].yaxis.grid(True)
        print ''

        ax[jdx-1].set_ylim(bottom=0,top=1.05)
        ax[jdx-1].set_xscale('log')
        #ax[idx,jdx-1].set_xlim(left=0,right=100)
        #ax[idx,jdx-1].set_xticks([])

    list_handlers = []
    for v in func_names:
        list_handlers.append(mline.Line2D([],[], color=func2style[v][0], linewidth=2 ))

    plt.figlegend(handles=list_handlers, labels=[function2name[f] for f in func_names], loc='upper left')
    fig.text(0.5, 0.04, xlabel_dict[extension], ha='center', size='xx-large')
    fig.text(0.04, 0.5, 'CCDF', va='center', rotation='vertical', size='xx-large')

else:
    fig = plt.figure()
    ax = fig.add_subplot(111)
    for kdx,initial in enumerate(dist_initials):
        for function in func_names:
            infile = open(initial+'_'+function+'_idx'+str(idx)+'_jdx'+str(jdx)+extension)
            actions = np.loadtxt(infile)

            #print tmp.keys()
            RUNS = len(actions)
            ecdf = ECDF(actions)
            ax.plot( ecdf.x, ecdf.y, func2style[function][0]+'-', linewidth=2 )
            ax.set_title(r'Bernoulli({:.3f}) vs. Bernoulli({:.3f}), $N=10^5$'.format(idx2mean(idx)/10, idx2mean(jdx)/10) )
            ax.yaxis.grid(True)

    list_handlers = []
    for v in func_names:
        list_handlers.append(mline.Line2D([],[], color=func2style[v][0], linewidth=2 ))

    #plt.figlegend(handles=list_handlers, labels=[function2name[f] for f in func_names], loc='upper left')
    plt.legend(list_handlers, [function2name[f] for f in func_names], loc='best')

    fig.text(0.5, 0.04, xlabel_dict[extension], ha='center', size='xx-large')
    fig.text(0.04, 0.5, 'CCDF', va='center', rotation='vertical', size='xx-large')

plt.savefig(initial+'_ts_vs_modifiedts.pdf')
