#include <math.h>

class Catoni {
    private:
        double est_mean = 0.0;
        void funcd(const double theta, double &fn, double &df);
        double rtsafe(const double x1, const double x2, const double xacc);
    public:
        double minus_log_delta, min_elem, max_elem, xacc, v;
        vector<double> data;

        Catoni( double delta, double _v, double _xacc ) :
            minus_log_delta(-log(delta)), v(_v), xacc(_xacc),
            min_elem(numeric_limits<double>::max()), max_elem(numeric_limits<double>::min())  {}
        double push( double x, bool compute = false );
        double psi( const double theta );

        void set_v( const double _v ){ v = _v; }
        //double summation( const double );
};
