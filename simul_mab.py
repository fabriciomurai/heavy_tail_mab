# pylint: disable=C0103
""" This module implements several MAB algorithms.

TO RUN:

    $ python simul_mab.py

    INPUTS: none. Simulation parameters are hard-coded in this script.
    OUTPUTS: saved in folder <RUNS>_<STEPS>/
             * <distribution initial>_<algorithm name>_idx<idx>_jdx<jdx>.atxt
                    Each line corresponds to one run. The line contains EITHER
                    the cumulative number of pulls to the worst arm, collected
                    at intervals of size <spacing> OR the total number of pulls
                    to the worst arm.

             * <distribution initial>_<algorithm name>_idx<idx>_jdx<jdx>.rtxt
                    Analogous to the above, but for expected regret.

             where the distribution parameters are calculated to match the mean
             of a Pareto distribution with exponent 2.1+idx*0.1 (2.1+jdx*0.1)
             and x_min = 2. The only exception is the Bernoulli distribution,
             whose mean is set to be the mean of the Pareto divided by 10.


SIMULATION PARAMETERS:

    dist_initials: list of chars from ['b','z','p','g'] for
                   Bernoulli, Zipf, Pareto and Geometric, respectively
    algo_names:    list of strings from ['ucb1','ucb1_normal','ts','modified_ts',
                   'modified_ts2','robust_ucb'] where
                    * 'ts' is Thompson Sampling FOR BERNOULLI BANDITS
                    * 'robust_ucb' uses Median-of-Means as an estimator
    NPROCESSES:
    RUNS:
    N:
    DEBUG:         if True, runs in serial mode. if False, runs in parallel mode,
                   in which case error msgs become useless for debugging.
"""

import numpy as np
from scipy.stats import zipf, geom, pareto, uniform, beta, bernoulli
import random
from collections import deque
from multiprocessing import Pool
import heapq
import scipy.integrate as integrate
import scipy.special as special
import ipdb
from mc_integration import beta_convolution
import warnings
warnings.filterwarnings('error')
import os
#import cProfile
#import sys
#import timeit

############# SIMULATION PARAMETERS #########################
dist_initials = ['b']
algo_names    = [ 'ts']

NPROCESSES = 4
RUNS = 100
N = 100000
DEBUG = False
if DEBUG:
    NPROCESSES = 1
    RUNS = 10
    N = 1000

# Output parameters
SAVE_FINAL_STEP_ONLY = True
spacing = 1000 # when SAVE_FINAL_STEP_ONLY == False, save every <spacing> steps

# Variable initialization
C = np.exp(1./8)
max_rwd = 0

if N < 1000:
    raise NameError('Number of steps N={} must be > 1000.'.format(N))
###########################################################

class MedianOfMeans:
    """Implements streaming algorithm to track median of means over blocks of numbers.

    Block size is given as input. This class stores observed numbers in a
    buffer until it becomes full. Then, it computes the block mean, which
    is in turn stored in a data structure composed by a max-heap and a min-
    heap. The cost of insertion in this data structure is O(logN) and the
    cost of retrieving the median is O(1).

    Attributes:
        block_size: size of blocks of numbers
        buf: buffer to store numbers prior to computing their mean
        max_heap: a max-heap that stores the smaller half of the means
        min_heap: a min-heap that stores the largest half
        counter: number of elements inserted so far
        median: current median

    """
    def __init__(self, block_size):
        """Inits median of means."""
        self.block_size = block_size
        self.min_heap = []
        self.max_heap = []
        self.buf = np.zeros(block_size)
        self.counter = 0
        self.m = 0

    def insert(self, element):
        """Process a new sample.

           Arguments:
               element: the new sample (a real number)

           Returns:
               The current median.
        """
        self.buf[self.counter%self.block_size] = element
        self.counter += 1

        if self.counter % self.block_size == 0:
            mean = np.mean(self.buf)
            self.buf[:] = 0
            diff = len(self.max_heap)-len(self.min_heap)
            if diff == 1:
                if mean < self.m:
                    heapq.heappush(self.min_heap,-heapq.heappop(self.max_heap))
                    heapq.heappush(self.max_heap,-mean)
                else:
                    heapq.heappush(self.min_heap, mean)

                self.m = (self.min_heap[0]-self.max_heap[0])/2.0

            elif diff == 0:
                if mean < self.m:
                    heapq.heappush(self.max_heap,-mean)
                    self.m = -self.max_heap[0]
                else:
                    heapq.heappush(self.min_heap, mean)
                    self.m = self.min_heap[0]

            elif diff == -1:
                if mean < self.m:
                    heapq.heappush(self.max_heap,-mean)
                else:
                    heapq.heappush(self.max_heap,-heapq.heappop(self.min_heap))
                    heapq.heappush(self.min_heap, mean)

                self.m = (self.min_heap[0]-self.max_heap[0])/2.0

        return self.m


    def median(self):
        """Retrieves the current median."""
        return self.m

class Rewards(object):
    """ Rewards implements a vector of random variables distributed according
        to one of four standard distributions, whose initials are provided in
        dist_name ('z': Zipf, 'g': Geometric, 'p': Pareto, 'b': Bernoulli). A
        single parameter is provided in the list dist_param. For Pareto,
        parameter x_min is fixed = 2.0.

       For efficiency purposes, it stores 1000 samples of each distribution in
       a buffer. Currently only Zipf, Geometric and Pareto distributions are
       implemented. Sampling from Pareto is implemented using the inverse-
       transform method, because there is a bug with the scipy implementation
       (as of version 0.11.0).

       Attributes:
           dist_name: list with initial of distribution name (in ['z','g','p'])
           dist_param: list with parameter of each distribution
           scale_param: scale parameter (only used when dist_name=='p')
           buf: list of buffers of samples for each distributions
           randvar: instance of random variable used for sampling
    """
    def __init__(self, dist_name, dist_param, scale_param=None):
        """Inits Rewards."""
        self.dist_name = dist_name
        self.dist_param = dist_param
        self.scale_param = scale_param
        self.buf = [[] for _ in dist_name]
        self.rv  = [[] for _ in dist_name]
        for ind in xrange(len(dist_name)):
            if dist_name[ind] == 'z':
                self.rv[ind] = zipf(dist_param[ind])
            elif dist_name[ind] == 'g':
                self.rv[ind] = geom(dist_param[ind])
            elif dist_name[ind] == 'p':
                #self.rv[ind] = pareto(dist_param[ind],scale=scale_param[ind])
                self.rv[ind] = uniform()
            elif dist_name[ind] == 'b':
                #self.rv[ind] = pareto(dist_param[ind],scale=scale_param[ind])
                self.rv[ind] = bernoulli(dist_param[ind])

    def variance_bound(self):
        """Return the maximum variance among the reward distributions."""
        var = []
        for ind, initial in enumerate(self.dist_name):
            if initial == 'p':
                alpha = self.dist_param[ind]
                var += [alpha*self.scale_param[ind]**2/((alpha-1)**2*(alpha-2))]

            else:
                #self.rv[ind] = pareto(dist_param[ind],scale=scale_param[ind])
                var += [self.rv[ind].stats(moments='var')]


        return max(var)

    def draw( self, ind ):
        """Draw sample from distribution whose index is ind."""
        if len(self.buf[ind]) == 0:
            if self.dist_name[ind] != 'p':
                self.buf[ind] = deque(self.rv[ind].rvs(size=1000))
            else:
                self.buf[ind] = deque(self.scale_param[ind]*self.rv[ind].rvs(size=1000)**(-1./self.dist_param[ind])) #inverse-transform method

        return self.buf[ind].popleft()

    def narms(self):
        """Get number of distributions."""
        return len(self.dist_param)


def median_of_means(x):
    """Robust UCB implementation."""
    delta = 0.05
    estimate = []
    for rewards in x:
        n = len(rewards)
        k = int(np.floor( min(n/2, 8*np.log10(C/delta) ) )) # chunk sizes
        N = int(np.floor(n/k)) # number of chunks
        estimate += [np.median(np.mean(np.array(np.split(np.array(rewards[:(N*k)]), N )),axis=1))]

    return estimate


def robust_ucb( r, N, v ):
    eps = 1
    delta = 0.05
    start_as_ucb1 = False
    actions = np.zeros(N)
    rewards = np.zeros(N)
    narms = r.narms()
    m = [MedianOfMeans(int(np.floor(1-8*np.log(delta)))) for _ in xrange(narms)]

    n = np.zeros(narms,dtype=np.int)
    mu = [0.0] * narms
    #x = [[] for _ in xrange(narms)]

    for t in xrange(1,N+1):
        must_pull = np.argwhere(n < np.ceil(32*np.log(t)+2))
        if must_pull.size > 0:
            if not start_as_ucb1:
                ind = random.choice(np.hstack(must_pull).tolist())
            else:
                scores = mu + np.sqrt(2*np.log(t)/n)
                #print 'Scores', scores
                ind = random.choice(np.argwhere(scores == np.amax(scores)))

        else:
            #scores = median_of_means(x) + pow(12*v,1/(1+eps))* pow(16*np.log(C*t*t)/s,eps/(1+eps))
            scores = mu + pow(12*v,1/(1+eps))* pow(16*np.log(C*t*t)/n,eps/(1+eps))
            ind = random.choice(np.hstack(np.argwhere(scores == scores.max())).tolist())

        d = r.draw(ind)
        #x[ind] += [d]
        mu[ind] = m[ind].insert(d)
        n[ind] += 1
        actions[t-1] = ind
        rewards[t-1] = d

    return (actions,np.cumsum(max_rwd-rewards))


def ucb1( r, N, v=0 ):
    """UCB1 implementation."""
    actions = np.zeros(N)
    rewards = np.zeros(N)
    narms = r.narms()

    n = np.zeros(narms,dtype=np.int)
    x = np.zeros(narms,dtype=np.float64)
    # initialization
    for ind in xrange(narms):
        d = r.draw(ind)
        x[ind] += d
        n[ind] += 1
        actions[ind] = ind
        rewards[ind] = d

    # main loop
    for i in xrange(narms,N):
        scores = x/n + np.sqrt(2*np.log(i)/n)
        #print 'Scores', scores
        ind = random.choice(np.argwhere(scores == np.amax(scores)))
        #print 'Chosen arm:', ind
        d = r.draw(ind)
        x[ind] += d
        n[ind] += 1
        actions[i] = ind
        rewards[i] = d

    return (actions,np.cumsum(max_rwd-rewards))

class RunningStat:
    """ Implements running average and variance. """
    def __init__(self):
        self.m_n = 0

    def clear(self):
        self.m_n = 0

    def push(self,x):
        self.m_n += 1
        if self.m_n == 1:
            self.m_oldM = self.m_newM = x
            self.m_oldS = 0.0
            return self.m_newM, None

        else:
            self.m_newM = self.m_oldM + (x-self.m_oldM)/self.m_n
            self.m_newS = self.m_oldS + (x-self.m_oldM)*(x-self.m_newM)
            self.m_oldM = self.m_newM
            self.m_oldS = self.m_newS
            return self.m_newM, self.m_newS/(self.m_n - 1)

    def numDataValues(self):
        return self.m_n

    def mean(self):
        if self.m_n > 0:
            return self.m_newM
        else:
            return None

    def variance(self):
        if self.m_n > 1:
            return self.m_newS/(self.m_n - 1)
        else:
            return None

    def standardDeviation(self):
        return np.sqrt(self.variance())

def ucb1_normal( r, N, v=0 ):
    """UCB1-Normal implementation."""
    actions = np.zeros(N)
    rewards = np.zeros(N)
    narms = r.narms()

    n = np.zeros(narms,dtype=np.int)
    x = np.zeros(narms,dtype=np.float64)
    var = np.zeros(narms,dtype=np.float64)
    rstat = [RunningStat() for _ in xrange(narms)]

    # initialization
    for i in xrange(2):
        for ind in xrange(narms):
            d = r.draw(ind)
            m,s = rstat[ind].push(d)
            n[ind] += 1
            x[ind] = m
            if s != None:
                var[ind] = s
            actions[i*narms+ind] = ind
            rewards[i*narms+ind] = d

    # main loop
    for i in xrange(narms*2,N):
        few_uses = np.argwhere( n < np.ceil(narms*np.log(i)) )
        if len(few_uses) > 0:
            ind = random.choice(few_uses)

        else:
            scores = x + np.sqrt(16*var*np.log(i-1)/n)
            ind = random.choice(np.argwhere(scores == np.amax(scores)))

        d = r.draw(ind)
        m,s = rstat[ind].push(d)
        n[ind] += 1
        x[ind] = m
        var[ind] = s
        actions[i] = ind
        rewards[i] = d

    return (actions,np.cumsum(max_rwd-rewards))

def MABsimul( mab_params ):
    """Performs one run of the MAB simulation with the desired parameters."""
    np.random.seed() # it is crucial to generate one random seed per process
    r = Rewards( mab_params['dist_name'],mab_params['dist_param'],mab_params['scale_param'])
    return globals()[mab_params['function']]( r, mab_params['N'], max(mab_params['v']) )

def farey(x, N):
    """ Implements the Farey algorithm for ration approximation.
        Input:  x in [0,1] and integer N.
        Output: a, b such that a/b is the best approximation of x for
                b=1,...,N. In case of ties, the smallest a, b are returned.
    """
    a, b = 0, 1
    c, d = 1, 1
    while (b <= N and d <= N):
        mediant = float(a+c)/(b+d)
        if x == mediant:
            if b + d <= N:
                return a+c, b+d
            elif d > b:
                return c, d
            else:
                return a, b
        elif x > mediant:
            a, b = a+c, b+d
        else:
            c, d = a+c, b+d

    if (b > N):
        return c, d
    else:
        return a, b

def ts( r, N, v=0 ):
    """Thompson Sampling implementation."""
    actions = np.zeros(N)
    rewards = np.zeros(N)
    narms = r.narms()
    prior = [[1,1] for i in xrange(narms)]
    for i in xrange(N):
        scores = np.array([beta.rvs(prior[j][0],prior[j][1]) for j in xrange(narms)])
        ind = np.argmax(scores)
        d = r.draw(ind)
        prior[ind][1-d] += 1
        actions[i] = ind
        rewards[i] = d

    return (np.cumsum(actions),np.cumsum(max_rwd-rewards))

class ActionHistory:
    """ ActionHistory is an efficient data structure to return the proportion
        of times the action 0 was taken in a time window. Used by modified_ts
    """
    def init__(self, narms):
        self.hist = []
        self.hist_size = 0
        self.nactions = [0 for i in xrange(narms)]

    def push(self, action):
        self.hist += [action]
        self.hist_size += 1
        self.nactions[action] += 1

    def setHistSize(self, new_size):
        total_size = len(self.hist) #100 [0,..,99]
        # suppose current history is [90,...,99]
        new_size = min(new_size,total_size)
        if new_size == self.hist_size:
            return
        if new_size > self.hist_size: #15 [85,...,99] > 10 [90,...,99]
            for i in xrange(total_size-new_size,total_size-self.hist_size):
                #print 'Adding hist[{}]:{}'.format(i,self.hist[i])
                self.nactions[self.hist[i]] += 1
        elif new_size < self.hist_size: #10 [90,...,99] < 15 [85,...,99]
            for i in xrange(total_size-self.hist_size,total_size-new_size):
                #print 'Removing hist[{}]:{}'.format(i,self.hist[i])
                self.nactions[self.hist[i]] -= 1
        self.hist_size = new_size

    def getProportion(self):
        return self.nactions[0]*1.0/max(1,sum(self.nactions))


def modified_ts2( r, N, v=0 ):
    """Batch Thompson Sampling implementation."""
    rewards = np.zeros(N)
    narms = r.narms()
    prior = [[1,1] for i in xrange(narms)]
    history = ActionHistory(narms)

    #print '#current\tdesired\twin_size'
    for i in xrange(N):
        desired_prop  = beta_convolution(prior)
        current_prop = history.getProportion()
        if current_prop < desired_prop:
            ind = 0
        else:
            ind = 1
        #print 'current:{:.2f}(W={}), desired:{:.2f}, ind:{}'.format(current_prop,history.hist_size, desired_prop,ind)
        #print '{:.2f} {:.2f} {}'.format(current_prop,desired_prop,history.hist_size)

        d = r.draw(ind)
        prior[ind][1-d] += 1
        history.push(ind)
        rewards[i] = d
        ##diff = abs(desired_prop-(1-desired_prop))
        ##history.setHistSize(int(round(1.0/max(diff,1e-2))))
        #_, denom = farey(desired_prop, 100)
        #history.setHistSize(denom)


    return (np.cumsum(history.hist),np.cumsum(max_rwd-rewards))

def modified_ts( r, N, v=0 ):
    """Batch Thompson Sampling implementation."""
    actions = np.zeros(N)
    rewards = np.zeros(N)
    narms = r.narms()
    prior = [[1,1] for i in xrange(narms)]

    i = 0
    while i < N:
        proportion,_  = integrate.quad(lambda z: (1-special.btdtr(prior[0][0],prior[0][1],z))*beta.pdf(z,prior[1][0],prior[1][1]), 0, 1 )
        numer, denom = farey(proportion, min(100,N-i) )
        if numer==0 and denom==1:
            denom=min(100,N-i)
        #print '{} = {}/{}'.format(proportion, numer, denom)
        for j in xrange(numer):
            d = r.draw(0)
            prior[0][1-d] += 1
            actions[i+j] = 0
            rewards[i+j] = d
        i += numer
        for j in xrange(denom-numer):
            d = r.draw(1)
            prior[1][1-d] += 1
            actions[i+j] = 1
            rewards[i+j] = d
        i += (denom-numer)

    return (np.cumsum(actions),np.cumsum(max_rwd-rewards))


def main():
    """Spawns multiple processes to simulate MAB algorithm."""
    initial2distrib = {"p":"Pareto","z":"Zipf","g":"Geometric","b":"Bernoulli"}
    directory = '{}_{}K'.format(RUNS,int(N/1000))
    if not os.path.exists(directory):
        os.makedirs(directory)
    print 'Output will be saved in {}/'.format(directory)
    if not DEBUG:
        pool = Pool(processes=NPROCESSES)

    # use Pareto parameters to compute other distributions' parameters so that the mean is the same
    indices = [(idx,jdx) for idx in xrange(0,10) for jdx in xrange(idx+1,10)]
    #indices = [(0,1)]
    for idx,jdx in indices:
        pareto_param   = [2.1+idx*0.1, 2.1+jdx*0.1]
        pareto_xmin = 2
        geom_param = []
        zipf_param = []
        bern_param = []

        # scale_param: minimum value assumed by Pareto r.v.
        scale_param = [pareto_xmin]*len(pareto_param)
        v = [] # will store the variance of each arm
        for alpha in pareto_param:
            mean = float(pareto.stats(alpha,scale=pareto_xmin,moments='m'))
            v += [float(pareto.stats(alpha,scale=pareto_xmin,moments='v'))]
            geom_param += [1.0/mean]
            bern_param += [mean/10.0]   # Bernoulli has a different mean: divide by 10 to obtain value in [0,1]
            print mean, alpha
            scale_param += [pareto_xmin]
            # end of pareto params

        #print pareto_param
        print v
        global max_rwd
        #max_rwd = 1.0/geom_param[0]
        print bern_param
        max_rwd = max(bern_param)

        mab_params = {'N':N,'v':v}
        ret = [0]*RUNS
        for kdx,initial in enumerate(dist_initials):
            mab_params['dist_name'] = [initial]*len(pareto_param)
            if initial == 'p': # pareto
                mab_params['dist_param']  = pareto_param
                mab_params['scale_param'] = scale_param

            else:
                mab_params['scale_param'] = None
                if initial == 'g': # geometric
                    mab_params['dist_param'] = geom_param
                elif initial == 'z': # zipf
                    raise NameError('Code to compute Zipf params has not been implemented yet.')
                    mab_params['dist_param'] = zipf_param
                elif initial == 'b': # bernoulli
                    mab_params['dist_param'] = bern_param
                else:
                    print 'Unknown distribution initial.'

            for algname in algo_names:
                print 'Simulating %s for %s distribution' % (algname,initial2distrib[initial])
                mab_params['function']=algname
                if not DEBUG:
                    ret = pool.map(MABsimul,[mab_params]*RUNS)
                else:
                    for i in xrange(RUNS):
                        ret[i] = MABsimul(mab_params)
                #cProfile.runctx('MABsimul(mab_params)', globals(), {'mab_params':mab_params}, 'restats')
                #sys.exit(0)

                if SAVE_FINAL_STEP_ONLY:
                    actions = np.zeros(RUNS)
                    regret = np.zeros(RUNS)
                    for run in xrange(RUNS):
                        actions[run] = ret[run][0][N-1]
                        regret[run] = ret[run][1][N-1]
                else:
                    regret = np.zeros((RUNS,int(N/spacing)))
                    actions = np.zeros((RUNS,int(N/spacing)))
                    for run in xrange(RUNS):
                        actions[run,:] = ret[run][0][::spacing]
                        regret[run,:]  = ret[run][1][::spacing]

                with open('{}/{}_{}_idx{}_jdx{}.atxt'.format(directory,initial,algname,idx,jdx), 'w') as f:
                    np.savetxt(f,actions)
                with open('{}/{}_{}_idx{}_jdx{}.rtxt'.format(directory,initial,algname,idx,jdx), 'w') as f:
                    np.savetxt(f,regret)
                del actions
                del regret



if __name__ == '__main__':
    #print timeit.timeit('main()',setup='from __main__ import main', number=1)
    main()
