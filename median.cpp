#include "median.hpp" 

OnlineMedian::OnlineMedian() {
    m = 0.0;
}

double OnlineMedian::push( double e ) {
    int sig = left.size()-right.size();
    switch(sig) {
        case 1: // There are more elements in left (max) heap
     
            if( e < m ) { // current element fits in left (max) heap
                // Remore top element from left heap and
                // insert into right heap
                double tmp = left.top();
                left.pop();
                right.push(tmp);
     
                // current element fits in left (max) heap
                left.push(e);
            } else {
                // current element fits in right (min) heap
                right.push(e);
            }
     
            // Both heaps are balanced
            m = (left.top()+right.top())/2.0;
            break;
     
        case 0: // The left and right heaps contain same number of elements
            if( e < m ) { // current element fits in left (max) heap
                left.push(e);
                m = left.top();
            } else {
                // current element fits in right (min) heap
                right.push(e);
                m = right.top();
            }
            break;
     
        case -1: // There are more elements in right (min) heap
     
            if( e < m ) { // current element fits in left (max) heap
                left.push(e);
            } else {
                // Remove top element from right heap and
                // insert into left heap
                double tmp = right.top();
                right.pop();
                left.push(tmp);
     
                // current element fits in right (min) heap
                right.push(e);
            }
            // Both heaps are balanced
            m = (left.top()+right.top())/2.0;
            break;
    }
    return m;
}

double OnlineMedian::getMedian() { return m; }

MedianOfMeans::MedianOfMeans( int _block_size ) : block_size(_block_size) {
    counter = 0;
    sum = 0.0;
}

double MedianOfMeans::push( double element ) {
    sum += element;
    counter++;

    double median;
    if( counter % block_size == 0 ) {
        median = m.push(sum/block_size);
        sum = 0.0;
    } else {
        median = m.getMedian();
    }
    return median;
}
 
//#include <iostream>
//int main(void) {
//    int block_size = 4;
//    MedianOfMeans mom( block_size );
//
//    for( int i=0; i < 100; i++ ) {
//        double m = mom.insert(i);
//
//        //if((i+1)%100 == 0)
//            cerr << i << ": " << m << endl;
//    }
//}
