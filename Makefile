CXXSRCDIR=.
CXXINCLUDE=-I${CXXSRCDIR}
CXXOBJDIR=build/objects/cc
CXXOPTS=-O3 -fpermissive -arch x86_64 -lstdc++
CPPFLAGS=-std=c++11
CXXDEBUG=-g -ggdb
CXX=g++

CXXCOMPILE=${CXX} ${CXXDEBUG} ${CPPFLAGS} ${CXXINCLUDE}
#CXXCOMPILE=${CXX} ${CPPFLAGS} ${CXXOPTS} ${CXXINCLUDE}

all: simul_mab sed_simul_mab

simul_mab: simul_mab.cpp
	${CXXCOMPILE} -o $@ $< median.cpp catoni.cpp -I. -lm -lpthread -L/opt/local/lib -I/opt/local/include

catoni: catoni.cpp
	${CXXCOMPILE} -o $@ $< -I. -lpthread -L/opt/local/lib -I/opt/local/include

bayesian_test: bayesian_test.cpp
	${CXXCOMPILE} -o $@ $< -I. -lm -L/opt/local/lib -I/opt/local/include

simul_vz: simul_vz.cpp
	${CXXCOMPILE} -o $@ $< median.cpp catoni.cpp -I. -lm -lpthread -L/opt/local/lib -I/opt/local/include

sed_simul_mab: sed_simul_mab.cpp sed/code/sed.o sed/code/kuiper.o
	${CXXCOMPILE} -o $@ $< sed/code/sed.o sed/code/kuiper.o  -I. -lm -lpthread -L/opt/local/lib -I/opt/local/include
