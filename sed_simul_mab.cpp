#include "sed/code/sed.hpp"
#include "ThreadPool/ThreadPool.h"
#include <string>
#include <fstream>
#include <sstream>
using namespace std;

#define kRUNS 2              // number of runs
#define kNARMS 2             // number of arms
#define kNTHREADS 1          // number of threads for parallel runs
#define kSPACING 1           // group actions and observed rewards every
#define kN 90                // number of steps in each run
#define INTERARRIVAL_TIME 3  // number of time units between arm pulls

#define SAVE_REWARD_HIST
#define DEBUG

class Results{
    private:
        int spacing;
        double offset;
        vector< vector<int> > actions;
        vector<double> rewards;
        vector<int> action_buf;
        double reward_buf;
        int64_t counter;

    public:
        Results( int64_t N, int _spacing=1, double _offset=0.0 ) : spacing(_spacing), offset(_offset) {
            counter = 0;
            rewards.resize(N/_spacing, 0.0);
            reward_buf = 0.0;

            actions.resize(kNARMS);
            action_buf.resize(kNARMS,0);
            for( auto&& i : actions )
                i.resize(N/_spacing, 0);
        }
        void push( int a, double r=0.0 ) {
            action_buf[a]++;
            reward_buf += offset-r;
            counter++;
            if( counter%spacing == 0 ) {
                int pos = counter/spacing - 1;
                if( pos > 0 ) {
                    rewards[pos] = rewards[pos-1];
                    for( int i=0; i < kNARMS; i++ )
                        actions[i][pos] = actions[i][pos-1];
                }
                rewards[pos] += reward_buf;
                for( int i=0; i < kNARMS; i++ )
                    actions[i][pos] += action_buf[i];

                reward_buf = 0.0;
                fill(action_buf.begin(), action_buf.end(), 0);
            }
        }
        vector<double> getRewards(void) { return rewards; }
        vector<int> getActions(void) {
            int last = actions[0].size()-1;
            vector<int> cumulative_actions;
            for( auto i:actions )
                cumulative_actions.push_back(i[last]);
            return cumulative_actions;
        }
};

class SEDReward {
    /* Differently than the Thompson Sampling implementation in
       simul_mab.cpp, here the same class (i.e., SEDReward) that computes
       posteriors is the class that returns rewards. In fact, the rewards
       are obtained through a "crystal ball" that allows us to see the value
       that the cascade will achieve by the end of the simulated process.
       However, these rewards cannot be used to update the posteriors.
       Instead, we advance the clock INTERARRIVAL_TIME units of time to
       collect more observations and update the posteriors. All those
       differences justify the need to implement this as a different
       program.
    */
    public:
        SEDReward(string hist_file, string curr_file, int _interarrival_time, int _horizon) :
            nsteps(0), interarrival_time(_interarrival_time), horizon(_horizon) {
            dsed.init(hist_file, curr_file);
            narms = dsed.numCascadesCurrent();
            counts = vector<int> (narms,0.0);
        }
        vector<double> drawMeans(int prediction_time_in_steps) {
            nsteps++;
            vector<double> samples(narms,0.0);

			// if any arm was never sampled, return vector of zeros
            for(auto const &n: counts)
                if(n == 0)
                    return samples;

			// otherwise, compute posterior and sample from it
            dsed.setRelativeTime((nsteps-1)*interarrival_time);
            samples = dsed.drawPredictions(prediction_time_in_steps*interarrival_time);
            return samples;
        }
        double pull(int arm_ind) {
            counts[arm_ind]++;
            int sample_path_ind = dsed.enableSamplePath(arm_ind, (nsteps-1)*interarrival_time);
			return dsed.getSamplePathState(arm_ind, sample_path_ind, horizon*interarrival_time);
        }
        int getNumArms() { return narms; }
    private:
        DynamicSED dsed;
        int interarrival_time;
        int nsteps;
        int narms;
		int horizon;
        vector<int> counts;

};

typedef Results (*MABalgorithm)(SEDReward *, int32_t N);

Results ThompsonSampling(SEDReward *r, int32_t N) {
    Results res(N,kSPACING);
    int narms = r->getNumArms();

    for( int t=0; t < N; t++ ) {
        // sample means
        vector<double> sampledMeans = r->drawMeans(N); // FIXME: what is the horizon?
        cerr << "Sampled means: ";
        for(auto d: sampledMeans)
            cerr << d << " ";
        cerr << endl;

        
        // get best arm
        double max_mean = -numeric_limits<double>::max();
        vector<int> best;
        for( int i=0; i < narms; i++ ) {
            if( sampledMeans[i] >= max_mean ) {
                if(sampledMeans[i] > max_mean) {
                    max_mean = sampledMeans[i];
                    best.clear();
                }
                best.push_back(i);
            }
        }
        int max_ind = best[rand() % best.size()];
        cerr << "t=" << t << " selected ind: " << max_ind << endl;

		// pull arm and update DynamicSED state
        double d = r->pull(max_ind);
        res.push(max_ind,d);
    }

    return res;
}

void compareAlgorithms() {
    string hist_file("histdata_samples"); // historical data
    string curr_file("histdata_samples"); // current cascades (each corresponds to an arm)

    const char *function_names[] = {"thompson"};
    vector<string> function_str(function_names, end(function_names));
    int64_t steps = kN;
    int interarrival_time = INTERARRIVAL_TIME;

    vector<MABalgorithm> functions;
    for( auto fstr: function_str ) {
        if( fstr == "thompson" )
            functions.push_back(ThompsonSampling);
        //else if( fstr == "ucb1" )
        //    functions.push_back(Ucb1);
        //else if( fstr == "ucb1_normal" )
        //    functions.push_back(Ucb1Normal);
        //else if( fstr == "robust_ucb" )
        //    functions.push_back(RobustUCBMoM);
        //else if( fstr == "catoni" )
        //    functions.push_back(RobustUCBCatoni);
        //else if( fstr == "vz" )
        //    functions.push_back(VakiliZhao);
        else {
            cerr << "Method " << fstr << "not recognized." << endl;
            exit(1);
        }
    }

    for( int fdx=0; fdx < functions.size(); fdx++ ) {
        ThreadPool pool(kNTHREADS);
        std::vector< std::future<Results> > results;
        MABalgorithm fun = functions[fdx];

        for( int32_t i = 0; i < kRUNS; i++ ) {
            results.emplace_back(
                pool.enqueue([fun, steps, hist_file, curr_file, interarrival_time] {
                    SEDReward *r = new SEDReward(hist_file, curr_file, interarrival_time, steps);
                    return fun(r,steps);
                })
            );
        }

        ostringstream oss;
        oss << curr_file << "_" << function_str[fdx] << ".atxt";
        cerr << "Saving output to " << oss.str() << endl;
        ofstream actfile(oss.str(), ofstream::out);
#ifdef SAVE_REWARD_HIST
        oss.str("");
        oss << curr_file << "_" << function_str[fdx] << ".rtxt";
        ofstream rwdfile(oss.str(), ofstream::out);
#endif
        
        for(auto && result: results) {
            auto res = result.get();
            actfile << res.getActions().back() << endl;
#ifdef SAVE_REWARD_HIST
            auto rewards = res.getRewards();
            for( int i = 0; i < rewards.size()-1; i++  )
                rwdfile << rewards[i] << ' ';
            rwdfile << rewards.back() << endl;
#endif
        }
        actfile.close();
#ifdef SAVE_REWARD_HIST
        rwdfile.close();
#endif
    }
}

int main(void) {
    compareAlgorithms();
    return 0;
}
