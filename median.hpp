#include <vector>
#include <queue>
using namespace std;
 
class OnlineMedian {
    private:
        priority_queue<double, vector<double>, less<double> > left;
        priority_queue<double, vector<double>, greater<double> > right;
        double m;
    public:
        OnlineMedian();
        double push( double e );
        double getMedian();
};

class MedianOfMeans {
    private:
        int block_size;
        int64_t counter;
        double sum;
        OnlineMedian m;
    public:
        MedianOfMeans( int _block_size );
        double push( double element );
};
