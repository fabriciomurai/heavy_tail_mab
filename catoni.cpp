#include <iostream>
#include <math.h>
#include <cassert>
#include "roots.h"
#include "catoni.hpp"


using namespace std;

/*#define kBUFFER_SIZE 1000
Catoni M( 0.05, -1, 1e-5 );

struct ParetoParams {
    double scale;
    double shape;
};

class RewardDistribution{
    protected:
        RandomLib::Random &r;
        vector<double> buf;
    private:

        virtual vector<double> _draw( int32_t ) {
            return vector<double>();
        }
    public:
        RewardDistribution( RandomLib::Random &_r ) : r(_r) {}
         double draw() {
            if( buf.empty() )
                buf = _draw( kBUFFER_SIZE );

            double x = buf.back();
            buf.pop_back();
            return x;
        }
};

class ParetoDistribution : public RewardDistribution {
    private:
        double scale;
        double shape;

        vector<double> _draw( int32_t nsamples ) {
            vector<double> vec(nsamples, 0);
            const double exponent = -1.0/shape;
            for( double& x : vec)
                x = scale*pow(r.Fixed(),exponent);
            return vec;
        }
    public:
        ParetoDistribution( RandomLib::Random &_r, struct ParetoParams pp ) :
            RewardDistribution(_r), scale(pp.scale), shape(pp.shape) {}
};*/


Doub Catoni::rtsafe(const Doub x1, const Doub x2, const Doub xacc) {
	const Int MAXIT=100;
	Doub xh,xl, fl, fh, _df;
        funcd(x1,fl,_df);
        funcd(x2,fh,_df);
	if ((fl > 0.0 && fh > 0.0) || (fl < 0.0 && fh < 0.0))
		throw("Root must be bracketed in rtsafe");
	if (fl == 0.0) return x1;
	if (fh == 0.0) return x2;
	if (fl < 0.0) {
		xl=x1;
		xh=x2;
	} else {
		xh=x1;
		xl=x2;
	}
	Doub rts=0.5*(x1+x2);
	Doub dxold=abs(x2-x1);
	Doub dx=dxold;
	Doub f, df;
        funcd(rts,f,df);
	for (Int j=0;j<MAXIT;j++) {
		if ((((rts-xh)*df-f)*((rts-xl)*df-f) > 0.0)
			|| (abs(2.0*f) > abs(dxold*df))) {
			dxold=dx;
			dx=0.5*(xh-xl);
			rts=xl+dx;
			if (xl == rts) return rts;
		} else {
			dxold=dx;
			dx=f/df;
			Doub temp=rts;
			rts -= dx;
			if (temp == rts) return rts;
		}
		if (abs(dx) < xacc) return rts;
                funcd(rts,f,df);
		if (f < 0.0)
			xl=rts;
		else
			xh=rts;
	}
	throw("Maximum number of iterations exceeded in rtsafe");
}

void Catoni::funcd(const double theta, double &fn, double &df) {
    double denom = data.size()*(v+2*v*minus_log_delta/(data.size()-2*minus_log_delta));
    double alpha = sqrt(2*minus_log_delta/denom);
    fn = 0.0;
    df = 0.0;
    for( auto x: data ) {
        double argument = alpha*(x-theta);
        double sign = argument < 0 ? -1: 1;
        double common = 1+abs(argument)+argument*(argument/2.0);

        fn += sign*log(common);
        df += sign*(1+abs(argument))/common;
    }
    df *= -alpha;
}


/*void summation(const double theta, double &fn, double &df) {
    double denom = M.data.size()*(M.v+2*M.v*M.minus_log_delta/(M.data.size()-2*M.minus_log_delta));
    double alpha = sqrt(2*M.minus_log_delta/denom);
    fn = 0.0;
    df = 0.0;
    for( auto x: M.data ) {
        double argument = alpha*(x-theta);
        double sign = argument < 0 ? -1: 1;
        double common = 1+abs(argument)+argument*(argument/2.0);

        fn += sign*log(common);
        df += sign*(1+abs(argument))/common;
    }
    df *= -alpha;
}*/


double Catoni::push( double x, bool compute ) {
    min_elem = std::min(min_elem,x);
    max_elem = std::max(max_elem,x);
    data.push_back(x);

    if( compute ) {
        if( data.size() > 2*minus_log_delta ) {
            double fl, fh, df;
            funcd(est_mean-xacc, fl, df);
            funcd(est_mean+xacc, fh, df);
            if( (fl > 0.0 && fh < 0.0) || (fl < 0.0 && fh > 0.0) ) {
                est_mean = rtsafe(est_mean-xacc, est_mean+xacc, xacc );
            } else {
                est_mean = rtsafe(min_elem, max_elem, xacc );
            }
        } else {
            est_mean = x;
        }
    }

    return est_mean;
}

//int main(void) {
//    struct ParetoParams pp;
//    RandomLib::Random rng;
//
//    pp.scale = 2.0;
//    pp.shape = 2.1;
//    double v = pp.shape*pow(pp.scale,2)/(pow(pp.shape-1,2)*(pp.shape-2));
//
//    ParetoDistribution r( rng, pp );
//    M.set_v( v );
//
//    for( int i = 0; i < 100000; i++ ) {
//        double d = r.draw();
//        M.push(d);
//    }
//    double d = r.draw();
//    cerr << M.push(d, true) << endl;
//
//    return 0;
//}
