import numpy as np
from scipy.stats import expon, uniform

def gen_data(outfile, exponents, N, HIST_SIZE):
    xmin = 0.1

    with open(outfile,'w') as f:
        for idx, alpha in enumerate(exponents):
            #beta = pareto.rvs(alpha,size=N)
            beta = xmin*uniform.rvs(size=N)**(-1./alpha)
            start_time = 0
            for jdx in xrange(N):
                intervals = expon.rvs(scale=1.0/beta[jdx],size=HIST_SIZE)
                timestamps = start_time+np.cumsum(intervals).astype(int)
                #values = np.arange(HIST_SIZE)+1
                #pairs =  np.vstack([timestamps,values]).T.flatten()
                text = '\t'.join(['{} {}'.format(t,kdx+1) for kdx, t in enumerate(list(timestamps))])
                f.write('#c{} #s{} {}\n'.format(idx, jdx, text))

                start_time += expon.rvs(scale=1.0/beta[jdx]*0.1*HIST_SIZE).astype(int)

            print 'Mean interarrival time: ', start_time/N

gen_data('histdata_samples', [2,3], 100, 1000)
