#include "sed.hpp"

void testSED() {
    SED sed(string("testdata/testdata_samples1"), string("testdata/testdata_samples2"));
    sed.save_data("testdata/out");
    sed.predict(sed.getSamples1()[0], 10, 0, 100, 10000, false, "testdata/out" );
}

void testDynamicSED() {
    // Initialized Dynamic SED using file histdata_samples both as the
    // historical data and the current cascades
    DynamicSED dsed("histdata_samples","histdata_samples");
    // Index of the cascade that will be predicted

    int mean_interarrival_time = 50;
    int delta_t1 = 500;
    int delta_t2 = 1000;

    vector<double> predictions;
    for(int curr_ind=0; curr_ind < dsed.numCascadesCurrent(); curr_ind++)
        for(int i=0; ; i++) {
            dsed.enableSamplePath(curr_ind, i*mean_interarrival_time);
            if(mean_interarrival_time*i == delta_t1) {
                fprintf( stdout, "Computing predictions based on %d first sample paths.\n", i+1);
                dsed.setRelativeTime(delta_t1);

                Pdf predicted = dsed.predictCascade(curr_ind, delta_t2);
                cerr << "# Predicted pdf" << endl;
                printPdf(predicted);
                break; // stops enabling additional sample paths
            }
        }
    //dsed.drawPredictions(delta_t2);

    ofstream f("currdata_forecast");
    for(auto const &d: predictions)
        f << d << " ";
    f.close();
}

int main(void) {
    //testSED();
    testDynamicSED();
    return 0;
}
