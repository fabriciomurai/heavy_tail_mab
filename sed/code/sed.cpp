#include <fstream>
#include <sstream>
#include <algorithm>
#include <set>
#include <random>
#include <limits>
#include <assert.h>
#include "kuiper.hpp"
#include "sed.hpp"
using namespace std;

//#define DEBUG

random_device rd;
mt19937 generator(rd());
uniform_real_distribution<> uni_dist(0, 1);
Kuiper *kp = new Kuiper();

double sampleFromPdf(Pdf p) {
    double u = uni_dist(generator);
    for(auto const &it: p) {
        u -= it.second;
        if(u <= 0)
            return it.first;
    }
    cerr << "sampleFromPdf: ERROR while sampling from pdf." << endl;
    exit(3);
}

bool pairCompare(const EOR& firstElem, EOR& secondElem) {
  return firstElem.first > secondElem.first;
}


void printStats(string outfileprefix, Stats stats, bool append=false) {
    ofstream f;
    auto mode = ofstream::out | append*(ofstream::app);

    f.open(outfileprefix+"_mean.txt", mode);
    for(auto const &x: stats.avg)
        f << x << " ";
    f << endl;
    f.close();

    f.open(outfileprefix+"_median.txt", mode);
    for(auto const &x: stats.med)
        f << x << " ";
    f << endl;
    f.close();

    f.open(outfileprefix+"_std.txt", mode);
    for(auto const &x: stats.std)
        f << x << " ";
    f << endl;
    f.close();

    f.open(outfileprefix+"_moment.txt", mode);
    for(auto const &x: stats.mon)
        f << x << " ";
    f << endl;
    f.close();
}

// Computes running aveage and variance
void RunningStat::push(double x) {
    m_n++;
    // See Knuth TAOCP vol 2, 3rd edition, page 232
    if (m_n == 1) {
        m_oldM = m_newM = x;
        m_oldS = 0.0;
    } else {
        m_newM = m_oldM + (x - m_oldM)/m_n;
        m_newS = m_oldS + (x - m_oldM)*(x - m_newM);

        // set up for next iteration
        m_oldM = m_newM; 
        m_oldS = m_newS;
    }
}

void loadSamples(string infile, vector<string> &cascadeids, map<string,vector<double> > &cascadeBin) {
    // input format: each line is: cascadeid\tnumNodes1\tnumNodes2...
    // return: cascadeids, cascadeBin
    // cascadeids = [cascadeid] # order of cascadeid
    // cascadeBin = dict(cascadeid -> [#nodes])*/
    string line;
    string cid;
    double x;

    cascadeids.clear();
    cascadeBin.clear();
    ifstream f(infile);
    while(getline(f, line)) {
        std::istringstream iss(line);
        iss >> cid;
        cascadeids.push_back(cid);
        while(iss >> x)
            cascadeBin[cid].push_back(x);
    }
    f.close();
}

// TODO(fabricio): save and load in binary format
void savePMatrix(DoubleMatrix pmatrix, string outfile) {
    ofstream f(outfile);
    f.flags(ios::scientific);
    for(int i=0; i < pmatrix.size(); i++) {
        if(i > 0) f << endl;
        f << pmatrix[i][0];
        for(int j=1; j < pmatrix[i].size(); j++) {
            f << " " << pmatrix[i][j];
        }
    }
    f.close();
}


DoubleMatrix readPMatrix(string infile) {
    DoubleMatrix pmatrix;
    ifstream f(infile);
    string line;
    while(getline(f, line)) {
        vector<double> row;
        double x;
        std::istringstream iss(line);
        while(iss >> x)
            row.push_back(x);
        pmatrix.push_back(row);

#ifdef DEBUG
        fprintf(stderr, "readPMatrix(%s): row.size = %lu\n", infile.c_str(), row.size());
#endif
    }
#ifdef DEBUG
    fprintf(stderr, "readPMatrix(%s): pmatrix.size = %lu\n", infile.c_str(), pmatrix.size());
#endif
    return pmatrix;
}

DoubleMatrix filterPMatrix(DoubleMatrix pmatrix, vector<int> index) {
    DoubleMatrix sub_pmatrix;
    for(int i=0; i < index.size(); i++) {
        vector<double> row;
        for(int j=0; j < index.size(); j++)
            row.push_back(pmatrix[i][j]);
        sub_pmatrix.push_back(row);
    }
    return sub_pmatrix;
}

// creates the probability distribution function from emprirical sample
// return: pdf = map() # value -> probability
Pdf create_pdf(vector<double> sample) {
    Pdf pdf;
    for(auto k:sample) {
        pdf[k] += 1.0;
    }
    int n = sample.size();
    for(auto &&kv:pdf)
        kv.second /= n;
    return pdf;
}

void printPdf(Pdf pdf) {
    fprintf(stdout, "#x\t\tP[X=x]\n");
    for(auto const &it: pdf) {
        fprintf(stdout, "%lf\t%lf\n", it.first, it.second);
    }
}

// given the probability distribution function, creates the CCDF
// input: pdf = map() # value -> probability
// return: ccdf = struct() # values = [], probs = []
Ccdf pdf_2_ccdf(Pdf pdf) {
    double cumdiff = 1.0;
    Ccdf ccdf;
    for(auto const &kv: pdf) {
        cumdiff -= kv.second;
        ccdf.values.push_back(kv.first);
        ccdf.probs.push_back(cumdiff);
    }
    if( ccdf.probs.back() > 1e-7 )
        fprintf(stderr, "[Warning] Zeroing ccdf[%lf] = %e.\n", ccdf.values.back(), ccdf.probs.back() );
    ccdf.probs.back() = 0;
    return ccdf;
}

// given a list of pdfs, and the similarity of them with a new process,
// compute the weigthed average of the pdfs as the prediction
Pdf weighted_average_pdf(vector<Pdf> pdfs, vector<double> weights = vector<double> ()) {
    set<double> newks;
    for(auto const &pdf: pdfs)
        for(auto const &kv: pdf)
            newks.insert(kv.first);

    double sumw = 0.0;
    if(weights.empty()) {
        sumw = 1.0*pdfs.size();
        weights.resize(pdfs.size(),1.0);
    } else {
        for(auto const &w: weights)
            sumw += w;
    }

    map<double,int> kmap;
    int count=0;
    for(auto const &k: newks)
        kmap[k] = count++;

    // put the pdfs of all neighbors into a single matrix
    vector<vector<double> > newpdfs(pdfs.size());
    for(int i=0; i<pdfs.size(); i++) {
        newpdfs[i].resize(newks.size());
        for(auto const &kv: pdfs[i])
            newpdfs[i][kmap[kv.first]] = kv.second;
    }

    Pdf newpdf;
    count=0;
    for(auto const &k: newks) {
        double sum=0.0;
        for(int i=0; i<newpdfs.size(); i++) {
            sum += newpdfs[i][count] * weights[i];
        }
        newpdf[k] = sum/sumw;
        count++;
    }
    return newpdf;
}

//  performs bootstrap sampling with the given numSamples & sample size
//  sampling is done in two steps: 1. pick an original sample in sampleList based on the probability pred_prob. 2. bootstrap sampling from the chosen sample with given size. 3. Repeat 1 & 2 for numSamples times.
//  sampleList = [[]] : the list of samples from knns
//  pred_prob = [] : the probability for choosing each neighbors in knns
//  if pred_prob = None: uniform sampling from the list of original sample
//  return: samples
//      samples : two dimensional arrays, samples[sampleid][values]
DoubleMatrix bootstrap_twostep(DoubleMatrix sampleList, int size=100, int numSamples=10000, vector<double> pred_prob = vector<double> ()) {
    DoubleMatrix samples(numSamples, vector<double> (size,0.0));
    vector<int> sids(numSamples,0);
    if(!pred_prob.empty()) {
        discrete_distribution<int> dist(pred_prob.begin(), pred_prob.end());
        for(auto &&sid: sids)
            sid = dist(generator);
    } else {
        uniform_int_distribution<int> dist(0,sampleList.size()-1);
        for(auto &&sid: sids)
            sid = dist(generator);
    }
    uniform_int_distribution<int> dist(0,sampleList[0].size());
    for(int i=0; i<numSamples; i++)
        for(int j=0; j<size; j++)
            samples[i][j] = sampleList[sids[i]][dist(generator)];

    return samples;
}

// performs two-step bootstrap sampling, and then computes statistics on the returned set of samples
// return: mean,median,std,2nd-moment
// each is an array, each element is for a sample
Stats stat_samples_twostep(DoubleMatrix samplesList, int size=100, int numSamples=10000, vector<double> pred_prob = vector<double> ()) {
    DoubleMatrix samples = bootstrap_twostep(samplesList, size, numSamples, pred_prob);
    assert(samples.size() == numSamples);
    Stats stats;
    for(auto const & sample: samples) {
        RunningStat rstat;
        for(auto const & x:sample)
            rstat.push(x);
        stats.avg.push_back(rstat.mean());
        stats.med.push_back(0.5*(sample[size/2]+sample[(size+1)/2]));
        stats.std.push_back(rstat.standardDeviation());
        stats.mon.push_back(rstat.variance()+rstat.mean()*rstat.mean());
    }
    return stats;
}

// maintains a historical dataset and uses it for prediction of partial new process
SED::SED(string samplefile1, string samplefile2, string pvaluefile1){
    vector<string> cids1, cids2;
    map<string, vector<double> > s1, s2;
    loadSamples(samplefile1, cids1, s1);
    loadSamples(samplefile2, cids2, s2);
    
    vector<string> copy_cids1 = cids1;
    vector<string> copy_cids2 = cids2;
    sort(copy_cids1.begin(), copy_cids1.end());
    sort(copy_cids2.begin(), copy_cids2.end());
    vector<string> cids(copy_cids1.size());
    vector<string>::iterator it;
    it = set_intersection(copy_cids1.begin(), copy_cids1.end(),
            copy_cids2.begin(), copy_cids2.end(), cids.begin());
    cids.resize(it-cids.begin());

    DoubleMatrix samples1, samples2;
    for(auto cid: cids) {
        samples1.push_back(s1[cid]);
        samples2.push_back(s2[cid]);
    }

    if(pvaluefile1 != "") {
        DoubleMatrix p1 = readPMatrix(pvaluefile1);
        vector<int> index;
        for(int i=0; i < cids1.size(); i++) {
            if( binary_search(cids.begin(), cids.end(), cids1[i]) )
                index.push_back(i);
        }
        p1 = filterPMatrix(p1, index);
        init(samples1, samples2, cids, p1);
    } else {
        init(samples1, samples2, cids);
    }
}
 //   given a partial new sample, makes prediction by comparing with samples1 to get EOR & predict by samples2
 //   alpha is the exponent to magnify the difference
 //   topk is the top chosen item for bootstrap sampleing, instead of using everything
 //       if topk = 0: use everthing
 //   if isUniform = True: Uninformed prediction using uniform sampling
 //   return: pdf,avg,med,std,mon
 //       pdf : the weighted average of the top similar pdfs
 //       avg,med,std,mon: these are the statistics using 2-step bootstrap sampling, with size bt_size, and bt_numSamples
Pdf SED::predict(vector<double> newSample, double alpha, int topk,
        int bt_size, int bt_numSamples, bool isUniform,
        string outfileprefix) {
    Pdf pred_pdf;
    Stats stats;
    if(isUniform) { // uniform prediction, no need for Kuiper's test
        pred_pdf = weighted_average_pdf(pdfs);
        stats = stat_samples_twostep(this->samples2,bt_size,bt_numSamples);
    } else { // perform prediction using Kuiper's test
        // compute the EOR against samples1, and get the top equivalent samples
        vector<EOR> topSim;
        for(int i=0; i<this->numSamples; i++) {
            double eor = pow(kp->kptwo(&(this->samples1[i][0]),&(newSample[0]),
                        this->samples1[i].size(),newSample.size())/
                    this->avgCol[i], alpha);
            topSim.emplace_back(eor,i);
        }
        if(topk > 0) {
            sort(topSim.begin(), topSim.end(), pairCompare);
            topSim.resize(topk);
        }

        // weights, pdfs, and samples used for prediction
        vector<double> ws;
        double sumws=0.0;
        for(auto const &fs: topSim) {
            sumws += fs.first;
            ws.push_back(fs.first);
        }
        for(auto &&w: ws)
            w /= sumws;
        vector<Pdf> chosenpdfs;
        DoubleMatrix chosenSamples;
        for(auto const &fs: topSim) {
            chosenpdfs.push_back(this->pdfs[fs.second]);
            chosenSamples.push_back(this->samples2[fs.second]);
        }

        // predict pdf
        pred_pdf = weighted_average_pdf(chosenpdfs, ws);
        stats =  stat_samples_twostep(chosenSamples,bt_size,bt_numSamples,ws);
    }

    if( outfileprefix != "") {
        DoubleMatrix newSample_vec;
        newSample_vec.push_back(newSample);
        Stats cur_stats = stat_samples_twostep(newSample_vec,bt_size, bt_numSamples);
        printStats(outfileprefix, cur_stats);
        printStats(outfileprefix, stats, true);
    }
    return pred_pdf;
}

//  given a sampleIndex or sampleId for a sample in the historical dataset, make prediction by compare with samples1 to get EOR & predict by samples2
//  alpha is the exponent to magnify the difference
//  topk is the top chosen item for bootstrap sampleing, instead of using everything
//      if topk = 0: use everthing
//  if isUniform = True: Uninformed prediction using uniform sampling
//  if bt_size = 0: use same bt_size as ground truth
//  return: pdf,avg,med,std,mon
//      pdf : the weighted average of the top similar pdfs
//      avg,med,std,mon: these are the statistics using 2-step bootstrap sampling, with size bt_size, and bt_numSamples
Pdf SED::predict_leave_one_out(int sampleIndex,
        string sampleId, double alpha, int topk,
        int bt_size, int bt_numSamples, bool isUniform,
        string outfileprefix) {

  //      If outfileprefix is not None, plot the results into files <outfileprefix>_ + '_ccdf.txt','_mean.txt','_med.txt','_std.txt','_mon.txt'
    if( (sampleIndex < 0 && sampleId.empty()) ||
        (sampleIndex < 0 || sampleIndex > this->numSamples) ||
        (!sampleId.empty() && this->sampleIndex.find(sampleId) == this->sampleIndex.end())) {
        fprintf(stderr, "Invalid sampleId %s or sampleIndex %d\n", sampleId.c_str(), sampleIndex);
        exit(2);
    }
    if(sampleIndex > 0 && !sampleId.empty() && sampleIndex != this->sampleIndex.find(sampleId)->second ) {
        fprintf(stderr, "sampledId %s and sampleIndex %d do not match. Provide one of them only\n",
                sampleId.c_str(), sampleIndex);
        exit(3);
    }
    if(!sampleId.empty())
        sampleIndex = this->sampleIndex[sampleId];
    if(bt_size == 0)
        bt_size = this->samples2[sampleIndex].size();

    vector<Pdf> chosenpdfs;
    DoubleMatrix chosenSamples;
    Pdf pred_pdf;
    Stats stats;
    if(isUniform) {
        for(int i=0; i<this->numSamples; i++) {
            if(i != sampleIndex) {
                chosenpdfs.push_back(this->pdfs[i]);
                chosenSamples.push_back(this->samples2[i]);
            }
        }

        // predict pdf
        pred_pdf = weighted_average_pdf(chosenpdfs);
        stats =  stat_samples_twostep(chosenSamples,bt_size,bt_numSamples);
    } else {
        vector<EOR> topSim;
        for(int i=0; i<this->numSamples; i++) {
            double eor = pow(this->pmatrix[sampleIndex][i]/
                    this->avgCol[i], alpha);
            topSim.emplace_back(eor,i);
        }
        if(topk > 0) {
            sort(topSim.begin(), topSim.end(), pairCompare);
            topSim.resize(topk);
        }

        // weights, pdfs, and samples used for prediction
        vector<double> ws;
        double sumws=0.0;
        for(auto const &fs: topSim) {
            sumws += fs.first;
            ws.push_back(fs.first);
        }
        for(auto &&w: ws)
            w /= sumws;
        vector<Pdf> chosenpdfs;
        DoubleMatrix chosenSamples;
        for(auto const &fs: topSim) {
            chosenpdfs.push_back(this->pdfs[fs.second]);
            chosenSamples.push_back(this->samples2[fs.second]);
        }

        // predict pdf
        pred_pdf = weighted_average_pdf(chosenpdfs, ws);
        stats =  stat_samples_twostep(chosenSamples,bt_size,bt_numSamples,ws);
    }
    if( outfileprefix != "") {
        DoubleMatrix samples1_vec, samples2_vec;
        samples1_vec.push_back(this->samples1[sampleIndex]);
        samples2_vec.push_back(this->samples2[sampleIndex]);
        Stats cur_stats = stat_samples_twostep(samples1_vec,bt_size, bt_numSamples);
        Stats fut_stats = stat_samples_twostep(samples2_vec,bt_size, bt_numSamples);

        if(!isUniform) {
            chosenpdfs.clear();
            for(int i=0; i<this->numSamples; i++) {
                if(i != sampleIndex) {
                    chosenpdfs.push_back(this->pdfs[i]);
                    chosenSamples.push_back(this->samples2[i]);
                }
            }
            // predict pdf
            Pdf unf_pdf = weighted_average_pdf(chosenpdfs);
            Stats unf_stats =  stat_samples_twostep(chosenSamples,bt_size,bt_numSamples);

            printStats(outfileprefix, cur_stats);
            printStats(outfileprefix, fut_stats, true);
            printStats(outfileprefix, stats, true);
            printStats(outfileprefix, stats, true);
        } else {
            printStats(outfileprefix, cur_stats);
            printStats(outfileprefix, stats, true);
        }
    }
    return pred_pdf;
}
// save the p-value matrix to file for faster loading in the future
// if saveSample == True: also save the two samples to file */
void SED::save_data(string outfileprefix, bool doSaveSamples) {
    savePMatrix(this->pmatrix, outfileprefix+"_pmatrix");
    if(doSaveSamples)
        cerr << "TODO: implement doSaveSamples" << endl;
}

void SED::init(DoubleMatrix samples1, DoubleMatrix samples2,
        vector<string> sampleids, DoubleMatrix pmatrix) {
    numSamples = samples1.size();
    if(numSamples == 0 || numSamples != samples2.size() || sampleids.size() != numSamples) {
        fprintf(stderr, "samples1.size() = %lu, samples2.size() = %lu, sampleids.size() = %lu\n",
                samples1.size(), samples2.size(), sampleids.size() );
        exit(1);
    }
    vector<Pdf> pdfs;
    vector<Ccdf> ccdfs;
    for(int i=0; i < numSamples; i++) {
        Pdf pdf = create_pdf(samples2[i]) ;
        pdfs.push_back(pdf);
        ccdfs.push_back(pdf_2_ccdf(pdf));
    }
#ifdef DEBUG
    cerr << "Finished computing pdfs and ccdfs." << endl;
#endif

    if(pmatrix.empty()) {
        pmatrix.resize(numSamples);
        for(auto &&row: pmatrix)
            row.resize(numSamples);
        for(int i=0; i < numSamples; i++) {
            pmatrix[i][i] = 1.0;
            for(int j=i+1; j < numSamples; j++) {
                //Kuiper_kptwo(kp, a, b, 0, 0);
                pmatrix[i][j] = pmatrix[j][i] = kp->kptwo(&(samples1[i][0]),
                        &(samples1[j][0]), samples1[i].size(), samples1[i].size() );
             }
            //cout << i << endl;
        }
#ifdef DEBUG
        cerr << "Finished Kuiper's tests" << endl;
#endif
    }
    // compute average p-value for each column 
    // this average will be used for the computation of EOR
    vector<double> avgCol(numSamples, 0.0);
    for(int j=0; j<numSamples; j++) {
        avgCol[j] = 0.0;
        for(int i=0; i<numSamples; i++)
            avgCol[j] += pmatrix[i][j];
        avgCol[j] /= numSamples;
    }

    this->pmatrix = pmatrix;
    this->avgCol = avgCol;
    this->samples1 = samples1;
    this->samples2 = samples2;
    this->pdfs = pdfs;
    this->ccdfs = ccdfs;
    if(!sampleids.empty()) {
        this->sampleids = sampleids;
    }
    else {
        this->sampleids.clear();
        for(int i=0; i < numSamples; i++)
            this->sampleids.push_back(to_string(i));
    }
    for(int i=0; i<this->sampleids.size(); i++)
        this->sampleIndex[this->sampleids[i]] = i;

#ifdef DEBUG
    cerr << "SED object created. Ready for prediction." << endl;
#endif
}


double SamplePath::getSnapshot(int delta_sec) {
    // Returns the most recent event e such that e.rel_start_time <= delta_sec
    assert(events.size()>0);
    int low=0;
    int high=events.size()-1;

    if(delta_sec > getObservedTime()) {
        fprintf(stderr, "[Warning] SamplePath::getSnapshot: relative \
time larger than observed time (%d > %d)\n", delta_sec, getObservedTime() );
        return events[high].value;
    }

    while(low <= high) {
        int mid=low+(high-low)/2;
        if( events[mid].rel_start_time <= delta_sec )
            low = mid+1;
        else
            high = mid-1;
    }
    return events[low-1].value;
}
void Cascade::addSamplePath(string sample_id, int abs_start_time) {
    if(sampleIndex.find(sample_id) == sampleIndex.end()) {
        samples.emplace_back(sample_id, abs_start_time);
        sampleIndex[sample_id] = samples.size()-1;
    } else {
        cerr << "Sample path " << sample_id << " already exists." << endl;
    }
}
void Cascade::addEvent(string sample_id, int abs_event_time, double value) {
    auto it = sampleIndex.find(sample_id);
    if( it != sampleIndex.end()) {
        fprintf( stderr, "Cascade::addEvent sample_id %s does not exist in cascade %s.\n",
                sample_id.c_str(), cascade_id.c_str());
        exit(2);
    }
    observed_time = max(observed_time, samples[it->second].addEvent(abs_event_time, value));
}
void Cascade::addSamplePath(SamplePath sample) {
    if(sampleIndex.find(sample.getId()) == sampleIndex.end()) {
        samples.push_back(sample);
        sampleIndex[sample.getId()] = samples.size()-1;
        num_enabled++;

        observed_time = max(observed_time, sample.getObservedTime());
    } else {
        cerr << "Sample path " << sample.getId() << " already exists." << endl;
    }
}

vector<double> Cascade::getSnapshot(vector<int> rel_times) {
    if(rel_times.size() > samples.size()) {
        fprintf(stderr, "Cascade::getSnapshot: number of samples in \
current cascade larger than number in process %s (%lu > %lu)\n",
            cascade_id.c_str(), rel_times.size(), samples.size());
        exit(2);
    }
    vector<double> snapshot;
    for(int i=0; i < rel_times.size(); i++) {
        if( rel_times[i] > observed_time )
            fprintf(stderr, "[Warning] Cascade::getSnapshot: relative \
time larger than observed time (%d > %d)\n", rel_times[i], observed_time );
        double value = samples[i].getSnapshot(rel_times[i]);
#ifdef DEBUG
        fprintf( stderr, "SamplePath::getSnapshot(%d)=%lf\n", rel_times[i], value);
#endif
        snapshot.push_back(value);
    }
    return snapshot;
}


// Enables next sample path in the list and records rel_time as the
// time the sample path was created relative to the beginning of the
// cascade
// Returns index of last sample path enabled
int PlaybackCascade::enableSamplePath(int rel_time) {
    num_enabled++;
#ifdef DEBUG
    fprintf( stderr, "enableSamplePath(%d): num_enabled=%d\n", rel_time, num_enabled);
#endif
    rel_start_time.push_back(rel_time);
    return num_enabled-1;
}
void PlaybackCascade::addSamplePath(SamplePath sample) {
    if(sampleIndex.find(sample.getId()) == sampleIndex.end()) {
        samples.push_back(sample);
        sampleIndex[sample.getId()] = samples.size()-1;
        //num_enabled++;
        observed_time = max(observed_time, sample.getObservedTime());
    } else {
        cerr << "Sample path " << sample.getId() << " already exists." << endl;
    }
}
vector<int> PlaybackCascade::getSamplePathAges(int cur_rel_time) {
    vector<int> rel_times(num_enabled);
    for(int i=0; i<num_enabled; i++) {
        rel_times[i] = cur_rel_time-rel_start_time[i];
    }
    return rel_times;
}

void DynamicSED::saveSnapshots(vector<Cascade> cascades, vector<int> relative_times, string outfile) {
    ofstream f(outfile);
#ifdef DEBUG
    cerr << "----------> outfile " << outfile << endl;
#endif
    for(Cascade c: cascades) {
        f << c.getCascadeId() << " ";
        for(double d: c.getSnapshot(relative_times) ) {
            f << " " << d;
        }
        f << endl;
    }
    f.close();
}


Pdf DynamicSED::predictCascade(int ind, int prediction_time) {
    auto curr = curr_cascades[ind];

    // 1. get relative times of the current cascade and save snapshots
    // of historical cascades
    vector<int> relative_times = curr.getSamplePathAges(rel_time);
    string samples1("histdata_samples1.txt");
    saveSnapshots(hist_cascades, relative_times, samples1);

    // 2. save snapshots of historical cascades at prediction time
    string samples2("histdata_samples2.txt");
    saveSnapshots(hist_cascades, vector<int> (relative_times.size(), prediction_time), samples2);

    // 3. run static SED
    SED sed(samples1, samples2);
    sed.save_data("saved");
    vector<double> newSample = curr.getSnapshot(relative_times);
    return sed.predict(newSample);
}
vector<double> DynamicSED::drawPredictions(int prediction_time) {
    vector<double> drawn_mean;

    for(auto curr: curr_cascades) {
        // 1. get relative times of the current cascade and save snapshots
        // of historical cascades
        vector<int> relative_times = curr.getSamplePathAges(rel_time);
#ifdef DEBUG
        for(auto d: relative_times)
            cerr << d << " ";
        cerr << endl;
#endif
        string samples1("histdata_samples1.txt");
        saveSnapshots(hist_cascades, relative_times, samples1);

        // 2. save snapshots of historical cascades at prediction time
        string samples2("histdata_samples2.txt");
        saveSnapshots(hist_cascades, vector<int> (relative_times.size(), prediction_time), samples2);

        // 3. run static SED
        SED sed(samples1, samples2);
        sed.save_data("saved");
        vector<double> newSample = curr.getSnapshot(relative_times);
        Pdf pdf = sed.predict(newSample);

        // 4. sample from Pdf
        drawn_mean.push_back(sampleFromPdf(pdf));

    }
#ifdef DEBUG
    for(auto d: drawn_mean)
        cerr << d << " ";
    cerr << endl;
#endif
    return drawn_mean;
}
vector<int> DynamicSED::numSamplePathsCurrent() {
    vector<int> nsamples;
    for(auto &curr: curr_cascades )
        nsamples.push_back(curr.numSamples());

    return nsamples;
}


void DynamicSED::loadCascades(string infile, bool historical) {
    // input format: each line is: cascadeid\tsampleid\t[timestamp\tvalue]\+...
    string line;
    ifstream f(infile);
    while(getline(f, line)) {
        string cid, sid;
        int abs_start_time;
        double value;
        std::istringstream iss(line);
        iss >> cid >> sid >> abs_start_time >> value;
        //fprintf( stderr, "cid %s, sid %s, %d %lf\n", cid.c_str(), sid.c_str(), abs_start_time, value );
        SamplePath s(sid, abs_start_time);

        s.addEvent(abs_start_time, value);
        while(iss >> abs_start_time >> value)
            s.addEvent(abs_start_time, value);

        if(historical) {
            if(hist_cascadeIndex.find(cid) == hist_cascadeIndex.end()) {
                hist_cascades.push_back(Cascade (cid));
                hist_cascadeIndex[cid] = hist_cascades.size()-1;
            }
            hist_cascades[hist_cascadeIndex[cid]].addSamplePath(s);
        } else {
            if(curr_cascadeIndex.find(cid) == curr_cascadeIndex.end()) {
                curr_cascades.push_back(PlaybackCascade (cid));
                curr_cascadeIndex[cid] = curr_cascades.size()-1;
            }
            curr_cascades[curr_cascadeIndex[cid]].addSamplePath(s);
        }
    }
    f.close();
}



//int main(void) {
//    //testSED();
//    testDynamicSED();
//    return 0;
//}
