#include <cmath>

class Kuiper{
    public:
      double qKP(double lambda);
      double kptwo(double* data1, double* data2, int n1, int n2);
      double kptwo_distance(double* data1, double* data2, int n1, int n2);
      void initkuiper();
};

//extern "C" {
//    Kuiper* Kuiper_new(){ return new Kuiper(); }
//    void initkuiper() { initkuiper(); }
//    double Kuiper_qKP(Kuiper* kuiper,double lambda){ return (kuiper->qKP(lambda)); }
//    double Kuiper_kptwo(Kuiper* kuiper,double* data1, double* data2, int n1, int n2){ return (kuiper->kptwo(data1, data2, n1, n2)); }
//    double Kuiper_kptwo_distance(Kuiper* kuiper,double* data1, double* data2, int n1, int n2){ return (kuiper->kptwo_distance(data1, data2, n1, n2)); }
//    double Kuiper_kptwo_log(Kuiper* kuiper,double* data1, double* data2, int n1, int n2){ return (log(kuiper->kptwo(data1, data2, n1, n2))); }
//}
