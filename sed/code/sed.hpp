#include <vector>
#include <map>
#include <string>
#include <iostream>
#include <cmath>
using namespace std;

typedef vector<vector<double> > DoubleMatrix;
typedef map<double,double> Pdf;
struct Ccdf {
    vector<double> values;
    vector<double> probs;
};

typedef pair<float,int> EOR;

struct Stats {
    vector<double> avg, med, std, mon;
};

class RunningStat
{
    public:
        RunningStat() : m_n(0) {}
        void clear() { m_n = 0; }
        int size() const { return m_n; }
        double mean() const {
            return (m_n > 0) ? m_newM : 0.0;
        }
        double variance() const {
            return ( (m_n > 1) ? m_newS/(m_n - 1) : 0.0 );
        }
        double sumSquaredDeviations() const {
            return m_newS;
        }
        double standardDeviation() const {
            return sqrt( variance() );
        }

        void push(double x);

    private:
        int m_n;
        double m_oldM, m_newM, m_oldS, m_newS;
};

// maintains a historical dataset and uses it for prediction of partial new process
class SED {
public:
    SED() : numSamples(0) { cerr << "Empty SED object created" << endl; }
    DoubleMatrix getSamples1() { return samples1; }

    SED(string samplefile1, string samplefile2, string pvaluefile1="");

     //   given a partial new sample, makes prediction by comparing with samples1 to get EOR & predict by samples2
     //   alpha is the exponent to magnify the difference
     //   topk is the top chosen item for bootstrap sampleing, instead of using everything
     //       if topk = 0: use everthing
     //   if isUniform = True: Uninformed prediction using uniform sampling
     //   return: pdf,avg,med,std,mon
     //       pdf : the weighted average of the top similar pdfs
     //       avg,med,std,mon: these are the statistics using 2-step bootstrap sampling, with size bt_size, and bt_numSamples
    Pdf predict(vector<double> newSample, double alpha=10.0, int topk=0,
            int bt_size=100, int bt_numSamples=10000, bool isUniform=false,
            string outfileprefix = string ());

    //  given a sampleIndex or sampleId for a sample in the historical dataset,
    //  make prediction by compare with samples1 to get EOR & predict by
    //  samples2
    //  alpha is the exponent to magnify the difference
    //  topk is the top chosen item for bootstrap sampleing, instead of using everything
    //      if topk = 0: use everthing
    //  if isUniform = True: Uninformed prediction using uniform sampling
    //  if bt_size = 0: use same bt_size as ground truth
    //  return: pdf,avg,med,std,mon
    //      pdf : the weighted average of the top similar pdfs
    //      avg,med,std,mon: these are the statistics using 2-step bootstrap sampling, with size bt_size, and bt_numSamples
    Pdf predict_leave_one_out(int sampleIndex = -1,
            string sampleId = string(), double alpha=10, int topk=0,
            int bt_size = 0, int bt_numSamples=10000, bool isUniform=false,
            string outfileprefix = string());
    // save the p-value matrix to file for faster loading in the future
    // if saveSample == True: also save the two samples to file */
    void save_data(string outfileprefix, bool doSaveSamples=false);

private:
    int numSamples;
    DoubleMatrix pmatrix;
    vector<double> avgCol;
    DoubleMatrix samples1, samples2;
    vector<Pdf> pdfs;
    vector<Ccdf> ccdfs;
    vector<string> sampleids;
    map<string,int> sampleIndex;

    // prepare the historical dataset, given the sample lists at two different times
    // samples1 = [samples]
    // samples2 = [samples]
    // samples = [values]        
    // sampleids = [id]: the list of names of each processes in the samples
    // if no input, create an empty class, which is used by other init methods
    // if pmatrix is None: compute the p-value matrix from Kuiper's tests
    // if sampleids is not provided, use sampleindex
    // both the sample list and the pvalue matrix are read from files
    // pvaluefile1 is for samples in samplefile1
    void init(DoubleMatrix samples1, DoubleMatrix samples2,
            vector<string> sampleids, DoubleMatrix pmatrix =  DoubleMatrix () );

};

/////////////////////// Classes required by Dynamic SED ////////////////////////
// Stores event as a pair (timestamp w.r.t. the beginning of sample path, value)
class Event {
    public:
        Event(int _rel_start_time, double _value) : rel_start_time(_rel_start_time), value(_value) {}
        int rel_start_time;
        double value;
};

// Stores sorted list of events, sample path id and absolute start time
class SamplePath {
    public:
        SamplePath(string _sample_id, int _abs_start_time) : sample_id(_sample_id), abs_start_time(_abs_start_time) {}
        int size() { return events.size(); }
        int getObservedTime() {
            if( !events.empty() )
                return events.back().rel_start_time;
            else
                return -1;
        }
        int getObservedTime(int cur_time) {
            return cur_time-abs_start_time;
        }
        int addEvent(int abs_event_time, double value) {
            //fprintf( stderr, "addEvent(%d, %lf)", abs_event_time, value );
            events.emplace_back(abs_event_time-abs_start_time, value);
            return abs_event_time-abs_start_time;
        }
        int getAbsStartTime() { return abs_start_time; }
        string getId() { return sample_id; }

        double getSnapshot(int delta_sec);
    private:
        string sample_id;
        int abs_start_time;
        vector<Event> events;
};

// Stores list of sample paths from the same cascade and provides interface
// to get snapshots
class Cascade {
    public:
        Cascade(string _cascade_id) : cascade_id(_cascade_id), observed_time(0), num_enabled(0) {}
        int numSamples() { return samples.size(); }
        string getCascadeId() { return cascade_id; }

        // Returns snapshot (vector) whose i-th entry contains the state of the
        // i-th sample path given timestamp rel_times[i] relative to the
        // beginning of the sample path.
        vector<double> getSnapshot(vector<int> rel_times);
        void addSamplePath(string sample_id, int abs_start_time);
        void addEvent(string sample_id, int abs_event_time, double value);
        void addSamplePath(SamplePath sample);

    protected:
        vector<SamplePath> samples;
        map<string,int> sampleIndex;
        string cascade_id;
        int observed_time;
        int num_enabled;

};

// Provides interface to "play" a cascade by enabling sample paths one at a time
class PlaybackCascade: public Cascade {
    public:
        PlaybackCascade(string _cascade_id) : Cascade(_cascade_id) {}
        // Enables next sample path in the list and records rel_time as the
        // time the sample path was created relative to the beginning of the
        // cascade
        int enableSamplePath(int rel_time);
        void addSamplePath(SamplePath sample);
        vector<int> getSamplePathAges(int cur_rel_time);
		double getSamplePathState(int sample_ind, int rel_time) { return samples[sample_ind].getSnapshot(rel_time-rel_start_time[sample_ind]);}
    private:
        vector<int> rel_start_time;
};

// Maintains a historical dataset and uses it for prediction of on-going new process
class DynamicSED {
public:
    DynamicSED() : rel_time(0.0) { cerr << "Empty DynamicSED object created" << endl; }
    DynamicSED(string hist_file, string curr_file) : rel_time(0.0) { init(hist_file, curr_file); }
    void init(string hist_file, string curr_file) {
        // loads cascades from hist_file as historical data and cascades from
        // curr_file as current processes.
        loadCascades(hist_file, true);
        loadCascades(curr_file, false);

        // TODO(fabricio): make sure that all processes in hist_file are
        // observed for at least the prediction time horizon. For now, assume
        // they are.
    }
    void setRelativeTime(int _rel_time) {
        //  sets current time relative to the
        //  begining of the cascade.
        //  IMPORTANT: call this function before calling drawPredictions(...)
        //             or predictCascade(...).

        rel_time = _rel_time;
    }

    void enableSamplePath(int _rel_time) {
        for(auto &curr: curr_cascades)
            curr.enableSamplePath(_rel_time);
    }

    int enableSamplePath(int cascade_ind, int _rel_time) {
        // "plays" another sample path from a given cascade, setting the time from the beginning
        // of the cascade to the beginning of the sample path to be rel_time.
        return curr_cascades[cascade_ind].enableSamplePath(_rel_time);
    }

    int enableSamplePath(string cascade_id, int _rel_time) { return curr_cascades[curr_cascadeIndex[cascade_id]].enableSamplePath(_rel_time); }

    double getSamplePathState(int cascade_ind, int sample_ind, int rel_time) {
        // returns value of sample path sample_id from cascade cascade_id at
        // time rel_time relative to the beginning of the sample path.
        return curr_cascades[cascade_ind].getSamplePathState(sample_ind, rel_time);
    }
    int numCascadesCurrent() { return curr_cascades.size(); }

    void saveSnapshots(vector<Cascade> cascades, vector<int> relative_times, string outfile);
    Pdf predictCascade(int ind, int prediction_time);
    vector<double> drawPredictions(int prediction_time);
    vector<int> numSamplePathsCurrent();


private:
    vector<Cascade> hist_cascades;
    vector<PlaybackCascade> curr_cascades;
    map<string,int> hist_cascadeIndex, curr_cascadeIndex;
    int rel_time;

    void loadCascades(string infile, bool historical=true);
};

