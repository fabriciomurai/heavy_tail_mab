#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <random>
//#include "RandomLib/Random.hpp"
#include "ThreadPool/ThreadPool.h"
#include "median.hpp"
#include "catoni.hpp"
#include <stdio.h>
#include <assert.h>

using namespace std;

//#define kRUNS 100
//#define kNARMS 2
//#define kNTHREADS 1
//#define kBUFFER_SIZE 1000
//#define kSPACING 1000
//#define kN 10000

#define kRUNS 1000         // number of runs
#define kNARMS 2           // number of arms
#define kNTHREADS 10       // number of threads for parallel runs
#define kBUFFER_SIZE 1000  // number of rv samples to be drawn at once
#define kSPACING 1         // group actions and observed rewards every kSPACING steps for outputing
#define kN 100             // number of steps in each run
#define SAVE_REWARD_HIST   // comment to save actions only

#define BAYESIAN_PARETO 0
#define BAYESIAN_LOGNORMAL 1
#define BAYESIAN_EXPONENTIAL 2

int bayesian_prior = 2;    // prior used by Thompson Sampling (0:Pareto, 1:LogNormal, 2:Exponential)

#define MAX( a, b ) ( (a) > (b) ? (a) : (b) )

double v = 0.0;
double u = 0.0;
double opt_eta = 0.0;
double max_rwd = 0.0;
double kappa = -1.0/3;

// Structs are used to store distribution parameters. This way is
// possible to run a MABAlgorithm providing only one extra argument.
struct ParetoParams {
    double scale;
    double shape;
};

struct GeometricParams {
    double p;
};

struct GammaParams {
    double alpha, beta;
    double inv_beta; // can be used instead of 1.0/beta
};

struct ExponentialParams {
    double lambda;
};

struct LogNormalParams {
    double mu, sigma;
};


// Keeps track of running average and variance
class RunningStat
{
    public:
        RunningStat() : m_n(0) {}
        void clear() { m_n = 0; }
        void push(double x) {
            m_n++;
            // See Knuth TAOCP vol 2, 3rd edition, page 232
            if (m_n == 1) {
                m_oldM = m_newM = x;
                m_oldS = 0.0;
            } else {
                m_newM = m_oldM + (x - m_oldM)/m_n;
                m_newS = m_oldS + (x - m_oldM)*(x - m_newM);

                // set up for next iteration
                m_oldM = m_newM; 
                m_oldS = m_newS;
            }
        }

        int size() const { return m_n; }
        double mean() const {
            return (m_n > 0) ? m_newM : 0.0;
        }
        double variance() const {
            return ( (m_n > 1) ? m_newS/(m_n - 1) : 0.0 );
        }
        double sumSquaredDeviations() const {
            return m_newS;
        }
        double standardDeviation() const {
            return sqrt( variance() );
        }

    private:
        int m_n;
        double m_oldM, m_newM, m_oldS, m_newS;
};

// Implements Truncated Mean estimator for Heavy-tailed distributions average 
class TruncatedMean {
    private:
        double m, u, p, a, kappa;
        vector<double> data;
        int nskipped = 0;
    public:
        TruncatedMean( double _u, double _p=2, double _kappa=0.0 ): u(_u), p(_p), m(0.0) {
            //kappa = (1-p)/p+1e-3;
            if( _kappa >= 0 )
                kappa = -1e-3;
            else
                kappa = _kappa;
            a = pow(4,p/(1-p))*pow(u,1/(1-p));
        }

        double push( double new_x, bool recompute=false ) {
            data.push_back(new_x);

            double u_by_log_inv_eps = u/(a*pow(data.size(),1+kappa*p/(p-1)));
            if( !recompute ) {
                if( abs(new_x) > pow(u_by_log_inv_eps*data.size(), 1/p) ) {
                    new_x = 0; // truncating
                    nskipped++;
                }
                m = m + (new_x-m)/data.size();
            } else {
                m = 0.0;
                nskipped = 0;
                for( int k = 0; k < data.size(); k++ ) {
                    double x = data[k];
                    if( abs(x) > pow(u_by_log_inv_eps*(k+1), 1/p) ) {
                        x = 0;
                        nskipped++;
                    }
                    m = m + (x-m)/(k+1);
                }
            }

            return m;
        }
        int getNumSkipped() { return nskipped; }
};


// RewardDistribution is the base class of more specific distributions.
class RewardDistribution{
    protected:
        mt19937 *r;
        vector<double> buf;
    private:

        virtual vector<double> _draw( int32_t ) {
            return vector<double>();
        }
    public:
        RewardDistribution(mt19937 &_r) : r(&_r) {}
        RewardDistribution(void) {}
        double draw() {
            if( buf.empty() )
                buf = _draw( kBUFFER_SIZE );

            double x = buf.back();
            buf.pop_back();
            return x;
        }
        virtual double get_v( void ) { return 0.0; }
        void set_rng(mt19937 &_r) { r = &_r; }
        virtual void printParameters() {}
};

// ParamDistribution is an interface for getting mean and variance of
// parametric distributions.
class ParamDistribution {
    public:
        virtual double getMean() { return 0.0; }
        virtual double getVariance() { return 0.0; }
};


// Specific distributions used to generate rewards inherit both from
// RewardDistribution and ParamDistribution.
class GeometricDistribution : public RewardDistribution, public ParamDistribution {
    private:
        double p;

        vector<double> _draw( int32_t nsamples ) {
            geometric_distribution<int> distribution(p);
            vector<double> vec(nsamples, 0);
            for( double& x : vec)
                x = distribution(*r);
            return vec;
        }
    public:
        GeometricDistribution( mt19937 &_r, struct GeometricParams gp ) :
             RewardDistribution(_r), p(gp.p) {}
        double getMean() { return 1/p; }
        double getVariance() { return p*(1-p); }
};




class ExponentialDistribution: public ParamDistribution, public RewardDistribution {
    protected:
        double lambda;
        double getLambda() { return lambda; }

        vector<double> _draw( int32_t nsamples ) {
            exponential_distribution<double> distribution(lambda);
            vector<double> vec(nsamples, 0);
            for( double& x : vec)
                x = distribution(*r);
            return vec;
        }
    public:
        ExponentialDistribution(double _lambda=0.0) : lambda(_lambda) {}
        ExponentialDistribution(mt19937 &r, double _lambda=0.0) : RewardDistribution(r), lambda(_lambda) {}
        double getMean() { return 1/lambda; }
        double getVariance() { return 1/(lambda*lambda); }
        void setLambda(double _lambda) { lambda = _lambda; }
};

class LogNormalDistribution: public ParamDistribution, public RewardDistribution {
    protected:
        double mu, sigma;
        double getMu() { return mu; }
        double getSigma() { return sigma; }

        vector<double> _draw( int32_t nsamples ) {
            //cerr << "Y" << getMean() << endl;
            lognormal_distribution<double> distribution(mu,sigma);
            vector<double> vec(nsamples, 0);
            RunningStat rstat;
            for( double& x : vec) {
                x = distribution(*r);
                rstat.push(x);
            }
            //cerr << "Z" << rstat.mean() << endl;
            return vec;
        }
    public:
        LogNormalDistribution(double _mu=0.0, double _sigma=1.0) : mu(_mu), sigma(_sigma) {}
        LogNormalDistribution(mt19937 &r, double _mu=0.0, double _sigma=1.0) : RewardDistribution(r), mu(_mu), sigma(_sigma) {}
        double getMean() { return exp(mu+(sigma/2)*sigma); }
        double getVariance() { return (exp(sigma*sigma)-1)*pow(getMean(),2); }
        void setMu(double _mu) { mu = _mu; }
        void setSigma(double _sigma) { sigma = _sigma; }
};

class ParetoDistribution: public ParamDistribution, public RewardDistribution {
    private:
        double scale, shape;
        vector<double> _draw( int32_t nsamples ) {
            uniform_real_distribution<double> distribution (0.0,1.0);
            vector<double> vec(nsamples, 0);
            const double exponent = -1.0/shape;
            for( double& x : vec)
                x = scale*pow(distribution(*r),exponent);
            return vec;
        }
    public:
        ParetoDistribution(double _scale=1.0, double _shape=2.0): scale(_scale), shape(_shape) {}
        ParetoDistribution(mt19937 &r, double _scale, double _shape): RewardDistribution(r), scale(_scale), shape(_shape) {}

        void setScale(double _scale) { scale = _scale; }
        void setShape(double _shape) { shape = _shape; }
        double getScale() { return scale; }
        double getShape() { return shape; }
        double getMean() { return (shape > 1) ? shape*scale/(shape-1) : numeric_limits<double>::max(); }
        double getVariance() { return shape*pow(scale,2)/(shape-2); }
        double getSecondCenteredMoment() { return shape*pow(scale,2)/(pow(shape-1,2)*(shape-2)); }
};

// ConjugatePrior is the base class used by Thompson Sampling.
class ConjugatePrior {
    protected:
        default_random_engine generator;
        mt19937 *r;
    public:
        ConjugatePrior() {}
        ConjugatePrior(mt19937 &r) : r(&r) {}
        virtual void push(vector<double> vec_x) {}
        virtual void push(double x) {}
        virtual ParamDistribution *sampleParameters() {
            return new ParamDistribution ();
        }
};


class BayesianPareto: public ConjugatePrior {
    private:
        GammaParams gamma;
        double xmin;
    public:
        BayesianPareto( mt19937 &r, double _xmin = 2.0 ) : ConjugatePrior(r), xmin(_xmin) {
            gamma.alpha = 5.0;
            gamma.inv_beta = 2.0;
        }
        void push( double x ) {
            if( x < xmin ) return;

            gamma.alpha += 1.0;
            gamma.inv_beta += log(x/xmin);
            //fprintf( stdout, "alpha:%lf, inv_beta=%lf\n", gamma.alpha, gamma.inv_beta);
        }
        void push( vector<double> vec ) {
            gamma.alpha += vec.size();
            for(auto x: vec)
                gamma.inv_beta += log(x/xmin);
        }
        ParamDistribution *sampleParameters() {
            ParetoDistribution *res = new ParetoDistribution();
            res->setScale(xmin);

            gamma_distribution<double> distribution(gamma.alpha,1.0/gamma.inv_beta);
            double shape = distribution(*r);
            //cout << "shape: " << shape << endl;
            //shape = gamma.alpha/gamma.inv_beta;
            //assert(shape > 2);
            res->setShape(shape);

            return res;
        }
};

class BayesianExponential: public ConjugatePrior {
    private:
        GammaParams gamma;
    public:
        BayesianExponential(void) {
            gamma.alpha = 3.0;
            gamma.beta = 1.0;

            generator.seed(time(NULL));
        }
        void push( vector<double> vec_x ) {
            double sum = 0.0;
            for( auto x: vec_x )
                sum += x;

            gamma.alpha += vec_x.size();
            gamma.beta /= (1+gamma.beta*sum);
        }
        void push( double x ) {
            gamma.alpha++;
            gamma.beta /= (1+gamma.beta*x);
        }
        ParamDistribution *sampleParameters() {
            ExponentialDistribution *res = new ExponentialDistribution();
            gamma_distribution<double> rateDist(gamma.alpha,gamma.beta);
            res->setLambda(rateDist(generator));

            return res;
        }
};

class BayesianLogNormal: public ConjugatePrior {
    private:
        GammaParams gamma;
        double mu, tau;
    public:
        BayesianLogNormal(void) : mu(0.0), tau(0.1) {
            gamma.alpha = 1.0;
            gamma.inv_beta = 1.0;

            generator.seed(time(NULL));
        }
        void push( vector<double> vec_x ) {
            RunningStat rstat;
            for( auto x: vec_x )
                rstat.push(log(x));

            double x_diff = rstat.mean()-mu;
            gamma.alpha += rstat.size()/2.0;
            gamma.inv_beta += 0.5*rstat.sumSquaredDeviations() + (0.5*tau*rstat.size()/(tau+rstat.size()))*x_diff*x_diff;
            mu += x_diff*(rstat.size()/(tau+rstat.size()));
            tau += rstat.size();
        }
        void push( double x ) {
            double x_diff = log(x)-mu;
            gamma.alpha += 0.5;
            gamma.inv_beta += (0.5*tau/(tau+1))*x_diff*x_diff;
            mu += x_diff/(tau+1);
            tau += 1.0;
        }
        ParamDistribution *sampleParameters() {
            LogNormalDistribution *res = new LogNormalDistribution();
            gamma_distribution<double> precisionDist(gamma.alpha,1.0/gamma.inv_beta);
            double precision = precisionDist(generator);
            res->setSigma(1.0/sqrt(precision));
            double stddev = 1.0/sqrt(precision)/sqrt(tau);
            normal_distribution<double> meanDist(mu, stddev);
            res->setMu(meanDist(generator));
            //res->setMu(mu);

            return res;
        }
        double getMu() { return mu; };
};

class Results{
    private:
        int spacing;
        double offset;
        vector< vector<int> > actions;
        vector<double> rewards;
        vector<int> action_buf;
        double reward_buf;
        int64_t counter;

    public:
        Results( int64_t N, int _spacing=1, double _offset=0.0 ) : spacing(_spacing), offset(_offset) {
            counter = 0;
            rewards.resize(N/_spacing, 0.0);
            reward_buf = 0.0;

            actions.resize(kNARMS);
            action_buf.resize(kNARMS,0);
            for( auto&& i : actions )
                i.resize(N/_spacing, 0);
        }
        void push( int a, double r ) {
            action_buf[a]++;
            reward_buf += offset-r;
            counter++;
            if( counter%spacing == 0 ) {
                int pos = counter/spacing - 1;
                if( pos > 0 ) {
                    rewards[pos] = rewards[pos-1];
                    for( int i=0; i < kNARMS; i++ )
                        actions[i][pos] = actions[i][pos-1];
                }
                rewards[pos] += reward_buf;
                for( int i=0; i < kNARMS; i++ )
                    actions[i][pos] += action_buf[i];

                reward_buf = 0.0;
                fill(action_buf.begin(), action_buf.end(), 0);
            }
        }
        vector<double> getRewards(void) { return rewards; }
        vector<int> getActions(void) {
            int last = actions[0].size()-1;
            vector<int> cumulative_actions;
            for( auto i:actions )
                cumulative_actions.push_back(i[last]);
            return cumulative_actions;
        }
};

typedef Results (*MABalgorithm)(RewardDistribution * [], int32_t N);

Results RobustUCBCatoni(RewardDistribution *r[], int32_t N) {
    Results res(N,kSPACING,max_rwd);
    vector<double> scores(kNARMS, 0.0);
    vector<double> mu(kNARMS, 0.0);
    vector<int> n(kNARMS, 0);
    vector<double> next_update(kNARMS, 0.0);

    double delta=0.05;
    double alpha=0.0;
    vector<Catoni *> m(kNARMS);
    for(int ind = 0; ind < kNARMS; ind++ )
        m[ind] = new Catoni(0.05, r[ind]->get_v(), 1e-5);

    for( int t=1; t < N+1; t++ ) {
        double max_score = numeric_limits<double>::min();
        vector<double> max_inds;
        int ind = -1;
        for( int i=0; i < kNARMS; i++ ) {
            if( n[i] < 4*log(1.0/delta) || n[i] < 8*log(t) ) {
                ind = i;
                break;
            }
            scores[i] = mu[i] + sqrt(8*v*log(t)/n[i]);
            if( scores[i] == max_score )
                max_inds.push_back(i);
            else if( scores[i] > max_score ) {
                max_score = scores[i];
                max_inds.clear();
                max_inds.push_back(i);
            }
        }

        if( ind == -1 )
            ind = max_inds[0]; // TODO: replace this by random

        double d = r[ind]->draw();
        n[ind] += 1;
        if( n[ind] >= next_update[ind] ) {
            mu[ind] = m[ind]->push(d, true);
            next_update[ind] = pow(n[ind],1.1);
            //next_update[ind] += 1.0;
        } else {
            mu[ind] = m[ind]->push(d, false);
        }
        res.push(ind, d);

        //if(t%100 == 0)
        //    for(int ind = 0; ind < kNARMS; ind++ ) 
        //        cerr << " n=" << n[ind] << " mu[" << ind << "] = " << mu[ind] << endl;
    }

    return res;
}

Results RobustUCBMoM(RewardDistribution *r[], int32_t N) {
    double eps=1.0;
    double delta=0.05;
    double C=exp(1.0/8);

    Results res(N,kSPACING,max_rwd);
    vector<double> scores(kNARMS, 0.0);
    vector<double> mu(kNARMS, 0.0);
    vector<int> n(kNARMS, 0);
    vector<MedianOfMeans *> m(kNARMS);
    for( auto&& i:m )
        i = new MedianOfMeans((int)floor(1-8*log(delta)));


    for( int t=1; t < N+1; t++ ) {
        double max_score = numeric_limits<double>::min();
        vector<double> max_inds;
        int ind = -1;
        for( int i=0; i < kNARMS; i++ ) {
            if( n[i] < ceil(32*log(t)+2) ) {
                ind = i;
                break;
            }
            scores[i] = mu[i] + pow(12*v,1.0/(1+eps)) * pow(16*log(C*t*t)/n[i],eps/(1+eps));
            if( scores[i] == max_score )
                max_inds.push_back(i);
            else if( scores[i] > max_score ) {
                max_score = scores[i];
                max_inds.clear();
                max_inds.push_back(i);
            }
        }

        if( ind == -1 )
            ind = max_inds[0]; // TODO: replace this by random

        double d = r[ind]->draw();
        n[ind] += 1;
        mu[ind] = m[ind]->push(d);
        res.push(ind, d);
    }
    return res;
}


Results Ucb1Normal(RewardDistribution *r[], int32_t N) {
    Results res(N,kSPACING,max_rwd);
    vector<double> x(kNARMS, 0.0);
    vector<double> var(kNARMS, 0.0);
    vector<double> scores(kNARMS, 0.0);
    vector<int> n(kNARMS, 0);
    vector<RunningStat> rstat(kNARMS);

    for( int t=0; t < 2*kNARMS; t++ ) {
        int ind = t%kNARMS;
        double d = r[ind]->draw();
        x[ind] += d;
        n[ind] += 1;
        rstat[ind].push(d);
        res.push(ind, d);
    }
    for( int ind=0; ind < kNARMS; ind++ ) {
        x[ind] = rstat[ind].mean();
        var[ind] = rstat[ind].variance();
    }

    for( int t=2*kNARMS; t < N; t++ ) {
        double max_score = numeric_limits<double>::min();
        vector<double> max_inds;
        int ind = -1;
        for( int i=0; i < kNARMS; i++ ) {
            if( n[i] < ceil(kNARMS*log(t)) ) {
                ind = i;
                break;
            }
            scores[i] = x[i] + sqrt(16*var[i]*log(t-1)/n[i]);
            if( scores[i] == max_score )
                max_inds.push_back(i);
            else if( scores[i] > max_score ) {
                max_score = scores[i];
                max_inds.clear();
                max_inds.push_back(i);
            }
        }

        if( ind == -1 )
            ind = max_inds[0]; // TODO: replace this by random

        double d = r[ind]->draw();
        rstat[ind].push(d);
        x[ind] = rstat[ind].mean();
        var[ind] = rstat[ind].variance();
        n[ind] += 1;
        res.push(ind, d);
    }
    return res;
}

Results VakiliZhao(RewardDistribution *r[], int32_t N) {
    assert(kNARMS == 2);

    Results res(N,kSPACING,max_rwd);
    double eta = 0.0;
    vector<double> x(kNARMS, 0.0); // caches the estimates of the mean rewards
    vector<int> n(kNARMS, 0);
    vector<TruncatedMean> m(kNARMS, TruncatedMean (u, 2.0, kappa) );
    vector<int> next_update(kNARMS,0.0);

    int t=0;
    while( t < N ) {
        // try each arm r in a round-robin fashion
        for( int ind=0; ind < kNARMS && t < N; ind++ ) {
            double d = r[ind]->draw();
            n[ind] += 1;
            if( n[ind] >= next_update[ind] ) {
                x[ind] = m[ind].push(d, true);
                //next_update[ind] = (int)(pow(n[ind],1.1));
                next_update[ind] = (int)(n[ind]*2);
                //next_update[ind]++;
            } else {
                x[ind] = m[ind].push(d, false);
            }
            res.push(ind, d);
            t++;
        }
        eta = (x[0]+x[1])/2.0;

        while( t < N ) {
            int max_ind = -1;
            double max_score = -numeric_limits<double>::max();
            // check for arms that satisfy: x > eta + n^kappa
            for( int ind=0; ind < kNARMS; ind++ ) {
                if( x[ind] > opt_eta + pow(n[ind],kappa) && x[ind] > max_score ) {
                    max_ind = ind;
                    max_score = x[ind];
                }
            }
            // if some arms satisfy the inequality above, take the best one
            if( max_ind != -1 ) {
                double d = r[max_ind]->draw();
                n[max_ind] += 1;
                if( n[max_ind] >= next_update[max_ind] ) {
                    x[max_ind] = m[max_ind].push(d, true);
                    //next_update[max_ind] = (int)(pow(n[max_ind],1.1));
                    next_update[max_ind] = (int)(n[max_ind]*2);
                    //next_update[max_ind]++;
                } else {
                    x[max_ind] = m[max_ind].push(d, false);
                }
                res.push(max_ind, d);
                eta = (x[0]+x[1])/2.0;

                t++;
            } else { // otherwise, go to round-robin stage
                break;
            }
        } 
    }
    //for( int ind=0; ind < kNARMS; ind++ )
    //    fprintf( stderr, "n[%d] = %d, skipped = %d\n", ind, n[ind], m[ind].getNumSkipped() );

    return res;
}




Results ThompsonSampling(RewardDistribution *r[], int32_t N) {
    Results res(N,kSPACING,max_rwd);
    random_device rd;
    mt19937 rng(rd());
    vector<ConjugatePrior *> estimatedDistrib(kNARMS);
    RunningStat rstat[2];
    ofstream arm1("arm1.txt"), arm2("arm2.txt");

    switch( bayesian_prior ) {
        case BAYESIAN_LOGNORMAL:
            for( int i=0; i < kNARMS; i++ )
                estimatedDistrib[i] = new BayesianLogNormal();
            break;
        case BAYESIAN_PARETO:
            for( int i=0; i < kNARMS; i++ )
                estimatedDistrib[i] = new BayesianPareto(rng);
            break;
        case BAYESIAN_EXPONENTIAL:
            for( int i=0; i < kNARMS; i++ )
                estimatedDistrib[i] = new BayesianExponential();
            break;
    }

    const int MIN_SAMPLES = 30;
    for( int t=0; t < MIN_SAMPLES*kNARMS;  t++ ) {
        int max_ind = t%kNARMS;
        double d = r[max_ind]->draw();
        if(max_ind == 0) {
            arm1 << d << endl;
        } else {
            arm2 << d << endl;
        }
        estimatedDistrib[max_ind]->push(d);
        res.push(max_ind, d);
    }


    for( int t=MIN_SAMPLES*kNARMS; t < N; t++ ) {
        //double min_exponent = numeric_limits<double>::max();
        double max_mean = -numeric_limits<double>::max();
        vector<int> best;
        for( int i=0; i < kNARMS; i++ ) {
            ParamDistribution *dist = estimatedDistrib[i]->sampleParameters();
            double mean = dist->getMean();
            //fprintf( stderr, "t=%d, mean[%d]=%lf\n", t, i, mean );
            if( mean >= max_mean ) {
                if(mean > max_mean) {
                    max_mean = mean;
                    best.clear();
                }
                best.push_back(i);
            }
            delete dist;
        }
        int max_ind = best[rand() % best.size()];
        double d = r[max_ind]->draw();
        if(max_ind == 0) {
            arm1 << d << endl;
        } else {
            arm2 << d << endl;
        }
        //rstat[max_ind].push(d);
        //fprintf( stderr, "t=%d, d=%lf (mean[%d] = %lf)\n", t, d, max_ind, rstat[max_ind].mean() );
        estimatedDistrib[max_ind]->push(d);
        res.push(max_ind, d);
    }

    arm1.close();
    arm2.close();
    return res;
}

Results Ucb1(RewardDistribution *r[], int32_t N) {
    Results res(N,kSPACING,max_rwd);
    vector<double> x(kNARMS, 0.0);
    vector<int> n(kNARMS, 0);
    vector<double> scores(kNARMS, 0.0);

    for( int ind=0; ind < kNARMS; ind++ ) {
        double d = r[ind]->draw();
        x[ind] += d;
        n[ind] += 1;
        res.push(ind, d);
    }

    for( int t=kNARMS; t < N; t++ ) {
        double max_score = numeric_limits<double>::min();
        vector<double> max_inds;
        for( int i=0; i < kNARMS; i++ ) {
            scores[i] = x[i]/n[i] + sqrt(2*log(t)/n[i]);
            if( scores[i] == max_score )
                max_inds.push_back(i);
            else if( scores[i] > max_score ) {
                max_score = scores[i];
                max_inds.clear();
                max_inds.push_back(i);
            }
        }

        int ind = max_inds[0]; // TODO: replace this by random
        double d = r[ind]->draw();
        x[ind] += d;
        n[ind] += 1;
        res.push(ind, d);
    }
    return res;
}

void thompsonSensitivityAnalysis() {
    char dist_initials[] = {'l'};
    //char prior_initials[] = {'p', 'l', 'e'};
    char prior_initials[] = {'p'};
    //vector<int> priors = { BAYESIAN_PARETO, BAYESIAN_LOGNORMAL, BAYESIAN_EXPONENTIAL };
    int64_t steps = kN;


    struct ParetoParams pareto_params[kNARMS];
    pareto_params[0].scale = 2.0; //b
    //pareto_params[0].shape = 2.14196175556; //a
    pareto_params[1].scale = 2.0;
    //pareto_params[1].shape = 3.68465846865;
    //

    for( int gdx = 2; gdx < 10; gdx++ ) {
    for( int pdx = gdx+1; pdx < 10; pdx++ ) {
        pareto_params[0].shape = 2.1 + 0.1*gdx;
        pareto_params[1].shape = 2.1 + 0.1*pdx;


        max_rwd = 0.0;
        v = 0.0;
        u = 0.0;
        opt_eta = 0.0;



        LogNormalParams lognormal_params[kNARMS];
        ExponentialParams exponential_params[kNARMS];
        int ind=0;
        for( auto pp: pareto_params ) {
            double mean = pp.shape*pp.scale/(pp.shape-1);
            double variance = pp.shape*pow(pp.scale,2)/(pow(pp.shape-1,2)*(pp.shape-2));

            cerr << mean << endl;
            opt_eta += mean;
            max_rwd = MAX(max_rwd, mean);
            v = MAX(v, variance);
            u = pp.shape*pow(pp.scale,2)/(pp.shape-2);

            exponential_params[ind].lambda = 1/mean;

            double tmp = log(variance/mean/mean+1);
            lognormal_params[ind].mu = log(mean) - 0.5*tmp;
            lognormal_params[ind].sigma = sqrt(tmp);

            cerr << "Pareto: " << pareto_params[ind].shape << ", " << pareto_params[ind].scale << endl;
            cerr << "LogNormal: " << lognormal_params[ind].mu << ", " << lognormal_params[ind].sigma << endl;
            cerr << "Exponential: " << exponential_params[ind].lambda << endl;

            ind++;
        }
        opt_eta /= 2.0;
        cerr << "max_rwd " << max_rwd << endl;
        cerr << "opt_eta " << opt_eta << endl;

        //cerr << "v " << v << endl;
        //cerr << "u " << u << endl;

        vector<MABalgorithm> functions;
        functions.push_back(ThompsonSampling);


        for( auto prior_initial: prior_initials ) {
        for( auto initial: dist_initials ) {
                ThreadPool pool(kNTHREADS);
                std::vector< std::future<Results> > results;
                MABalgorithm fun = functions[0];

                for( int32_t i = 0; i < kRUNS; i++ ) {
                    results.emplace_back(
                        pool.enqueue([fun, steps, i, initial, prior_initial, pareto_params, lognormal_params, exponential_params] {
                            random_device rd;
                            mt19937 rng(rd());
                            RewardDistribution *r[kNARMS];

                            if( initial == 'p' ) {
                                for( int32_t j = 0; j < kNARMS; j++ )
                                    r[j] = new ParetoDistribution( rng, pareto_params[j].scale, pareto_params[j].shape );
                            } else if( initial == 'l'){
                                for( int32_t j = 0; j < kNARMS; j++ )
                                    r[j] = new LogNormalDistribution( rng, lognormal_params[j].mu, lognormal_params[j].sigma );
                            } else if( initial == 'e'){
                                for( int32_t j = 0; j < kNARMS; j++ )
                                    r[j] = new ExponentialDistribution( rng, exponential_params[j].lambda );
                            } else{
                                cerr << "Distribution initial '" << initial << "'not recognized" << endl;
                                exit(1);
                            }

                            if(prior_initial == 'p') {
                                bayesian_prior = BAYESIAN_PARETO;
                            } else if(prior_initial == 'l') {
                                bayesian_prior = BAYESIAN_LOGNORMAL;
                            } else if(prior_initial == 'e') {
                                bayesian_prior = BAYESIAN_EXPONENTIAL;
                            } else{
                                cerr << "Prior distribution initial '" << prior_initial << "'not recognized" << endl;
                                exit(1);
                            }

                            return fun(r,steps);
                        })
                    );
                }

                char outfile[100];
                sprintf( outfile, "thompson_idx%d_jdx%d_true%c_prior%c.atxt", gdx, pdx, initial, prior_initial );
                cerr << "Saving output to " << outfile << endl;
                ofstream actfile(outfile, ofstream::out);
#ifdef SAVE_REWARD_HIST
                sprintf( outfile, "thompson_idx%d_jdx%d_true%c_prior%c.rtxt", gdx, pdx, initial, prior_initial );
                ofstream rwdfile(outfile, ofstream::out);
#endif
                
                for(auto && result: results) {
                    auto res = result.get();
                    actfile << res.getActions().back() << endl;
#ifdef SAVE_REWARD_HIST
                    auto rewards = res.getRewards();
                    for( int i = 0; i < rewards.size()-1; i++  )
                        rwdfile << rewards[i] << ',';
                    rwdfile << rewards.back() << endl;
#endif
                }
                actfile.close();
#ifdef SAVE_REWARD_HIST
                rwdfile.close();
#endif
        }
        }
    }
    }
}

void compareAlgorithms() {
    char dist_initials[] = {'p'};
    //const char *function_names[] = {"catoni","ucb1_normal","ucb1"};
    //const char *function_names[] = {"thompson", "ucb1", "ucb1_normal", "vz", "catoni"};
    const char *function_names[] = { "ucb1"};
    vector<int> priors = { BAYESIAN_PARETO, BAYESIAN_LOGNORMAL, BAYESIAN_EXPONENTIAL };
    vector<string> function_str(function_names, end(function_names));
    int64_t steps = kN;


    ParetoParams pareto_params[kNARMS];
    LogNormalParams lognormal_params[kNARMS];
    ExponentialParams exponential_params[kNARMS];
    GeometricParams geometric_params[kNARMS];
    pareto_params[0].scale = 2.0; //b
    //pareto_params[0].shape = 2.14196175556; //a
    pareto_params[1].scale = 2.0;
    //pareto_params[1].shape = 3.68465846865;
    //
    geometric_params[0].p = 0.5;
    geometric_params[1].p = 0.5;

    for( int gdx = 0; gdx < 10; gdx++ ) {
    for( int pdx = gdx+1; pdx < 10; pdx++ ) {
        pareto_params[0].shape = 2.1 + 0.1*gdx;
        pareto_params[1].shape = 2.1 + 0.1*pdx;

        max_rwd = 0.0;
        v = 0.0;
        u = 0.0;
        opt_eta = 0.0;

        int ind=0;
        for( auto pp: pareto_params ) {
            double mean = pp.shape*pp.scale/(pp.shape-1);
            double variance = pp.shape*pow(pp.scale,2)/(pow(pp.shape-1,2)*(pp.shape-2));

            cerr << mean << endl;
            opt_eta += mean;
            max_rwd = MAX(max_rwd, mean);
            v = MAX(v, variance);
            u = pp.shape*pow(pp.scale,2)/(pp.shape-2);

            exponential_params[ind].lambda = 1/mean;

            double tmp = log(variance/mean/mean+1);
            lognormal_params[ind].mu = log(mean) - 0.5*tmp;
            lognormal_params[ind].sigma = sqrt(tmp);

            cerr << "Pareto: " << pareto_params[ind].shape << ", " << pareto_params[ind].scale << endl;
            cerr << "LogNormal: " << lognormal_params[ind].mu << ", " << lognormal_params[ind].sigma << endl;
            cerr << "Exponential: " << exponential_params[ind].lambda << endl;

            ind++;
        }
        opt_eta /= 2.0;

        cerr << "max_rwd " << max_rwd << endl;
        cerr << "opt_eta " << opt_eta << endl;
        cerr << "v " << v << endl;
        cerr << "u " << u << endl;
        //v = 0.01;


        vector<MABalgorithm> functions;
        for( auto fstr: function_str ) {
            if( fstr == "ucb1" )
                functions.push_back(Ucb1);
            else if( fstr == "ucb1_normal" )
                functions.push_back(Ucb1Normal);
            else if( fstr == "robust_ucb" )
                functions.push_back(RobustUCBMoM);
            else if( fstr == "catoni" )
                functions.push_back(RobustUCBCatoni);
            else if( fstr == "vz" )
                functions.push_back(VakiliZhao);
            else if( fstr == "thompson" )
                functions.push_back(ThompsonSampling);

        }


        for( auto initial: dist_initials ) {
            for( int fdx=0; fdx < functions.size(); fdx++ ) {
                ThreadPool pool(kNTHREADS);
                std::vector< std::future<Results> > results;
                MABalgorithm fun = functions[fdx];

                for( int32_t i = 0; i < kRUNS; i++ ) {
                    results.emplace_back(
                        pool.enqueue([fun, steps, i, initial, lognormal_params, exponential_params, pareto_params, geometric_params] {
                            mt19937 rng(i);
                            RewardDistribution *r[kNARMS];

                            if( initial == 'p' ) {
                                for( int32_t j = 0; j < kNARMS; j++ )
                                    r[j] = new ParetoDistribution( rng, pareto_params[j].scale, pareto_params[j].shape );
                            } else if( initial == 'l'){
                                for( int32_t j = 0; j < kNARMS; j++ )
                                    r[j] = new LogNormalDistribution( rng, lognormal_params[j].mu, lognormal_params[j].sigma );
                            } else if( initial == 'e'){
                                for( int32_t j = 0; j < kNARMS; j++ )
                                    r[j] = new ExponentialDistribution( rng, exponential_params[j].lambda );
                            } else{
                                cerr << "Distribution initial '" << initial << "'not recognized" << endl;
                                exit(1);
                            }

                            return fun(r,steps);
                        })
                    );
                }

                ostringstream oss;
                oss << initial << "_" << function_str[fdx] << "_idx" << gdx << "_jdx" << pdx << ".atxt";
                cerr << "Saving output to " << oss.str() << endl;
                ofstream actfile(oss.str(), ofstream::out);
#ifdef SAVE_REWARD_HIST
                oss.str("");
                oss << initial << "_" << function_str[fdx] << "_idx" << gdx << "_jdx" << pdx << ".rtxt";
                ofstream rwdfile(oss.str(), ofstream::out);
#endif
                
                for(auto && result: results) {
                    auto res = result.get();
                    actfile << res.getActions().back() << endl;
#ifdef SAVE_REWARD_HIST
                    auto rewards = res.getRewards();
                    for( int i = 0; i < rewards.size()-1; i++  )
                        rwdfile << rewards[i] << ',';
                    rwdfile << rewards.back() << endl;
#endif
                }
                actfile.close();
#ifdef SAVE_REWARD_HIST
                rwdfile.close();
#endif
            }
        }
    }
    }
}





int main(void) {
    compareAlgorithms();
    //thompsonSensitivityAnalysis();
    return 0;
}
